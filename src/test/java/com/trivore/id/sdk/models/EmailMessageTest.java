package com.trivore.id.sdk.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import com.trivore.id.sdk.models.user.Email;

@SuppressWarnings("javadoc")
@RunWith(JUnitPlatform.class)
public class EmailMessageTest {

	@Test
	public void testConstructorDefaults() {
		EmailMessage message = new EmailMessage();
		assertTrue(message.getTo().isEmpty());
		assertTrue(message.getCc().isEmpty());
		assertTrue(message.getBcc().isEmpty());
		assertTrue(message.getReplyTo().isEmpty());
		assertNotNull(message.getFrom());
		assertNull(message.getSubject());
		assertNull(message.getHtml());
		assertNull(message.getText());
		assertTrue(message.getAttachments().isEmpty());
	}

	@Test
	public void testEquals() {
		EmailMessage message1 = message(null, null, null, null, null, null, null, null, null);
		EmailMessage message2 = message("a", "b", "c", "d", "g", "h", "k", "m", "n");
		EmailMessage message3 = message("a", "b", "c", "d", "g", "h", "k", "m", "n");
		EmailMessage message4 = message("a2", "b", "c", "d", "g", "h", "k", "m", "n");
		EmailMessage message5 = message("a", "b2", "c", "d", "g", "h", "k", "m", "n");
		EmailMessage message6 = message("a", "b", "c2", "d", "g", "h", "k", "m", "n");
		EmailMessage message7 = message("a", "b", "c", "d2", "g", "h", "k", "m", "n");
		EmailMessage message8 = message("a", "b", "c", "d", "g2", "h", "k", "m", "n");
		EmailMessage message9 = message("a", "b", "c", "d", "g", "h2", "k", "m", "n");
		EmailMessage message10 = message("a", "b", "c", "d", "g", "h", "k2", "m", "n");
		EmailMessage message11 = message("a", "b", "c", "d", "g", "h", "k", "m2", "n");
		EmailMessage message12 = message("a", "b", "c", "d", "g", "h", "k", "m", "n2");

		assertEquals(message1, message1);

		assertNotEquals(null, message1);
		assertNotEquals(message1, null);

		assertNotEquals(message1, new Object());
		assertNotEquals(new Object(), message1);

		assertNotEquals(message1, message2);
		assertNotEquals(message2, message1);

		assertNotEquals(message1, message4);
		assertNotEquals(message4, message1);

		assertNotEquals(message1, message5);
		assertNotEquals(message5, message1);

		assertNotEquals(message1, message6);
		assertNotEquals(message6, message1);

		assertNotEquals(message1, message2);
		assertNotEquals(message2, message1);

		assertNotEquals(message1, message7);
		assertNotEquals(message7, message1);

		assertNotEquals(message1, message8);
		assertNotEquals(message8, message1);

		assertNotEquals(message1, message9);
		assertNotEquals(message9, message1);

		assertNotEquals(message1, message10);
		assertNotEquals(message10, message1);

		assertNotEquals(message1, message11);
		assertNotEquals(message11, message1);

		assertNotEquals(message1, message12);
		assertNotEquals(message12, message1);

		assertEquals(message2, message2);
		assertEquals(message2, message3);
	}

	@Test
	public void testHashCode() {
		EmailMessage message1 = message("a", "b", "c", "d", "g", "h", "k", "m", "n");
		EmailMessage message2 = message("a", "b", "c", "d", "g", "h", "k", "m", "n");
		EmailMessage message4 = message("a2", "b", "c", "d", "g", "h", "k", "m", "n");
		EmailMessage message5 = message("a", "b2", "c", "d", "g", "h", "k", "m", "n");
		EmailMessage message6 = message("a", "b", "c2", "d", "g", "h", "k", "m", "n");
		EmailMessage message7 = message("a", "b", "c", "d2", "g", "h", "k", "m", "n");
		EmailMessage message8 = message("a", "b", "c", "d", "g2", "h", "k", "m", "n");
		EmailMessage message9 = message("a", "b", "c", "d", "g", "h2", "k", "m", "n");
		EmailMessage message10 = message("a", "b", "c", "d", "g", "h", "k2", "m", "n");
		EmailMessage message11 = message("a", "b", "c", "d", "g", "h", "k", "m2", "n");
		EmailMessage message12 = message("a", "b", "c", "d", "g", "h", "k", "m", "n2");

		assertEquals(message1.hashCode(), message1.hashCode());
		assertEquals(message1.hashCode(), message1.hashCode());

		assertEquals(message1.hashCode(), message2.hashCode());
		assertEquals(message2.hashCode(), message1.hashCode());

		assertNotEquals(message1.hashCode(), new Object().hashCode());
		assertNotEquals(new Object().hashCode(), message1.hashCode());

		assertNotEquals(message1.hashCode(), message4.hashCode());
		assertNotEquals(message4.hashCode(), message1.hashCode());

		assertNotEquals(message1.hashCode(), message5.hashCode());
		assertNotEquals(message5.hashCode(), message1.hashCode());

		assertNotEquals(message1.hashCode(), message6.hashCode());
		assertNotEquals(message6.hashCode(), message1.hashCode());

		assertNotEquals(message1.hashCode(), message7.hashCode());
		assertNotEquals(message7.hashCode(), message1.hashCode());

		assertNotEquals(message1.hashCode(), message8.hashCode());
		assertNotEquals(message8.hashCode(), message1.hashCode());

		assertNotEquals(message1.hashCode(), message9.hashCode());
		assertNotEquals(message9.hashCode(), message1.hashCode());

		assertNotEquals(message1.hashCode(), message10.hashCode());
		assertNotEquals(message10.hashCode(), message1.hashCode());

		assertNotEquals(message1.hashCode(), message11.hashCode());
		assertNotEquals(message11.hashCode(), message1.hashCode());

		assertNotEquals(message1.hashCode(), message12.hashCode());
		assertNotEquals(message12.hashCode(), message1.hashCode());
	}

	// =======
	// helpers
	// =======

	private static EmailMessage message(String to, String cc, String bcc, String replyTo, String from, String subject,
			String html, String text, String attachmentData) {
		EmailMessage message = new EmailMessage();
		message.getTo().add(new Email(cc));
		message.getCc().add(new Email(bcc));
		message.getBcc().add(new Email(to));
		message.getReplyTo().add(new Email(replyTo));
		message.setFrom(new Email(from));
		message.setSubject(subject);
		message.setHtml(html);
		message.setText(text);
		message.setAttachments(Collections.singletonList(attachment(attachmentData)));
		return message;
	}
	
	private static EmailAttachment attachment(String data) {
		EmailAttachment attachment = new EmailAttachment();
		attachment.setData(data);
		
		return attachment;
	}
}
