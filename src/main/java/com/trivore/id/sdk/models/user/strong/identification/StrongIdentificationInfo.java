package com.trivore.id.sdk.models.user.strong.identification;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents a user's strong identification info.
 * <p>
 * Used to get the user's strong identification info from Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class StrongIdentificationInfo implements Serializable {

	private String method;
	private List<String> identificationDocuments;
	private String otherExplanation;
	private String personalId;
	private String remarks;
	private String identifier;
	private String time;
	private String firstTime;
	private int count;
	private boolean identified;
	private String validTo;
	private Boolean expired;
	private String timestamp;

	/**
	 * Get method used to strongly identify user.
	 * <p>
	 * IN_PERSON, SUOMI_FI, SUOMI_FI_VALTUUDET or USER_API.
	 * </p>
	 *
	 * @return method used to strongly identify user
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Set method used to strongly identify user.
	 * <p>
	 * IN_PERSON, SUOMI_FI, SUOMI_FI_VALTUUDET or USER_API.
	 * </p>
	 *
	 * @param method method used to strongly identify user
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * Get documents used during identification.
	 *
	 * @return documents used during identification
	 */
	public List<String> getIdentificationDocuments() {
		if (identificationDocuments == null) {
			identificationDocuments = new ArrayList<>();
		}
		return identificationDocuments;
	}

	/**
	 * Set documents used during identification.
	 *
	 * @param identificationDocuments documents used during identification
	 */
	public void setIdentificationDocuments(List<String> identificationDocuments) {
		this.identificationDocuments = identificationDocuments;
	}

	/**
	 * Get other explanation related to identification.
	 *
	 * @return other explanation related to identification
	 */
	public String getOtherExplanation() {
		return otherExplanation;
	}

	/**
	 * Set other explanation related to identification.
	 *
	 * @param otherExplanation other explanation related to identification
	 */
	public void setOtherExplanation(String otherExplanation) {
		this.otherExplanation = otherExplanation;
	}

	/**
	 * Get user's personal ID.
	 * <p>
	 * May be censored if viewer doesn't have permission ACCOUNT_VIEW_PERSONAL_ID.
	 * </p>
	 *
	 * @return user's personal ID
	 */
	public String getPersonalId() {
		return personalId;
	}

	/**
	 * Set user's personal ID.
	 * <p>
	 * May be censored if viewer doesn't have permission ACCOUNT_VIEW_PERSONAL_ID.
	 * </p>
	 *
	 * @param personalId user's personal ID
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	/**
	 * Get remarks related to last strong identification event.
	 *
	 * @return remarks related to last strong identification event
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * Set remarks related to last strong identification event.
	 *
	 * @param remarks remarks related to last strong identification event
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * Get user name, client ID, or other who performed last identification.
	 *
	 * @return user name, client ID, or other who performed last identification
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * Set user name, client ID, or other who performed last identification.
	 *
	 * @param identifier user name, client ID, or other who performed last
	 *                   identification
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * Get timestamp when user was strongly identified.
	 *
	 * @return timestamp when user was strongly identified
	 */
	public String getTime() {
		return time;
	}

	/**
	 * Set timestamp when user was strongly identified.
	 *
	 * @param time timestamp when user was strongly identified
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * Get first time when user was strongly identified.
	 *
	 * @return first time when user was strongly identified
	 */
	public String getFirstTime() {
		return firstTime;
	}

	/**
	 * Set first time when user was strongly identified.
	 *
	 * @param firstTime first time when user was strongly identified
	 */
	public void setFirstTime(String firstTime) {
		this.firstTime = firstTime;
	}

	/**
	 * Get number of times user has been strongly identified.
	 *
	 * @return number of times user has been strongly identified
	 */
	public int getCount() {
		return count;
	}

	/**
	 * Set number of times user has been strongly identified.
	 *
	 * @param count number of times user has been strongly identified
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * Get identification status.
	 * <p>
	 * Has user been strongly identified.
	 * </p>
	 *
	 * @return identification status
	 */
	public boolean isIdentified() {
		return identified;
	}

	/**
	 * Set identification status.
	 * <p>
	 * Has user been strongly identified.
	 * </p>
	 *
	 * @param identified identification status
	 */
	public void setIdentified(boolean identified) {
		this.identified = identified;
	}
	
	/**
	 * The strong identification is valid until this date and time. Generated
	 * property. Not filterable.
	 *
	 * @return the validTo
	 */
	public String getValidTo() {
		return validTo;
	}
	
	/**
	 * The strong identification is valid until this date and time.
	 *
	 * @param validTo the validTo
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
	
	/**
	 * Has the strong identification expired.
	 *
	 * @return the expired
	 */
	public Boolean getExpired() {
		return expired;
	}
	
	/**
	 * Has the strong identification expired.
	 *
	 * @param expired the expired
	 */
	public void setExpired(Boolean expired) {
		this.expired = expired;
	}
	
	/**
	 * @return the timestamp when user was strongly identified
	 */
	public String getTimestamp() {
		return timestamp;
	}
	
	/**
	 * @param timestamp the timestamp when user was strongly identified
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof StrongIdentificationInfo)) {
			return false;
		}
		StrongIdentificationInfo that = (StrongIdentificationInfo) o;
		return count == that.count &&
				identified == that.identified &&
				Objects.equals(method, that.method) &&
				Objects.equals(identificationDocuments, that.identificationDocuments) &&
				Objects.equals(otherExplanation, that.otherExplanation) &&
				Objects.equals(personalId, that.personalId) &&
				Objects.equals(remarks, that.remarks) &&
				Objects.equals(identifier, that.identifier) &&
				Objects.equals(time, that.time) &&
				Objects.equals(firstTime, that.firstTime) &&
				Objects.equals(validTo, that.validTo) &&
				Objects.equals(expired, that.expired) &&
				Objects.equals(timestamp, that.timestamp);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(method, identificationDocuments, otherExplanation, personalId, remarks,
				identifier, time, firstTime, count, identified, validTo, expired, timestamp);
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("StrongIdentificationInfo{");
		sb.append("method='").append(method).append('\'');
		sb.append(", identificationDocuments=").append(identificationDocuments);
		sb.append(", otherExplanation='").append(otherExplanation).append('\'');
		sb.append(", personalId='").append(personalId).append('\'');
		sb.append(", remarks='").append(remarks).append('\'');
		sb.append(", identifier='").append(identifier).append('\'');
		sb.append(", time='").append(time).append('\'');
		sb.append(", firstTime='").append(firstTime).append('\'');
		sb.append(", count=").append(count);
		sb.append(", identified=").append(identified);
		sb.append(", validTo='").append(validTo).append('\'');
		sb.append(", expired=").append(expired);
		sb.append(", timestamp='").append(timestamp).append('\'');
		sb.append('}');
		return sb.toString();
	}
	
}
