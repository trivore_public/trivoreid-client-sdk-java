package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents a person's name.
 * <p>
 * Used to specify names for individual person's in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Name implements Serializable {

	private String givenName;
	private String middleName;
	private String familyName;

	/**
	 * Construct a new name.
	 */
	public Name() {
		// ...
	}

	/**
	 * Get user's given (first) name or names.
	 *
	 * @return returns given (first) name or names
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * Set user's given (first) name or names.
	 *
	 * @param givenName given (first) name or names
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/**
	 * Get user's middle name or names.
	 *
	 * @return returns middle name or names
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * Set user's middle name or names.
	 *
	 * @param middleName middle name or names
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * Get user's family (last) name or names.
	 *
	 * @return returns family (last) name or names
	 */
	public String getFamilyName() {
		return familyName;
	}

	/**
	 * Set user's family (last) name or names.
	 *
	 * @param familyName family (last) name or names
	 */
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(givenName, middleName, familyName);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Name)) {
			return false;
		}
		Name o = (Name) obj;
		return Objects.equals(givenName, o.givenName) //
				&& Objects.equals(middleName, o.middleName) //
				&& Objects.equals(familyName, o.familyName);
	}

	@Override
	public String toString() {
		return "Name [givenName=" + givenName + ", middleName=" + middleName + ", familyName=" + familyName + "]";
	}

}
