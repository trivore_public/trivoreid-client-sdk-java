package com.trivore.id.sdk.models.user.student;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents a student state info.
 * <p>
 * Used to specify a student state info in the Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentStateInfo implements Serializable {

	private StudentStatus state;
	private String studentFrom;
	private String studentTo;
	private String updated;
	private boolean lastQuerySuccess;
	private String lastQueryError;
	private String source;

	/**
	 * Construct new student state info.
	 */
	public StudentStateInfo() {
		// ...
	}

	/**
	 * @return the student state
	 */
	public StudentStatus getState() {
		return state;
	}

	/**
	 * @param state the student state to set
	 */
	public void setState(StudentStatus state) {
		this.state = state;
	}

	/**
	 * @return the date when user becomes student
	 */
	public String getStudentFrom() {
		return studentFrom;
	}

	/**
	 * @param studentFrom the date when user becomes student to set
	 */
	public void setStudentFrom(String studentFrom) {
		this.studentFrom = studentFrom;
	}

	/**
	 * @return the date when user stops being a student
	 */
	public String getStudentTo() {
		return studentTo;
	}

	/**
	 * @param studentTo the date when user stops being a student to set
	 */
	public void setStudentTo(String studentTo) {
		this.studentTo = studentTo;
	}

	/**
	 * @return timestamp when student status was last updated
	 */
	public String getUpdated() {
		return updated;
	}

	/**
	 * @param updated timestamp when student status was last updated
	 */
	public void setUpdated(String updated) {
		this.updated = updated;
	}

	/**
	 * Get the last query success.
	 * <p>
	 * True if last query from original source was successful. If false, the student
	 * information was not updated and may be stale. Client may use the previously
	 * known user information if it is not too old, or show an error message to the
	 * user if necessary, and try again later
	 * </p>
	 *
	 * @return the last query success
	 */
	public boolean isLastQuerySuccess() {
		return lastQuerySuccess;
	}

	/**
	 * Set the last query success.
	 * <p>
	 * True if last query from original source was successful. If false, the student
	 * information was not updated and may be stale. Client may use the previously
	 * known user information if it is not too old, or show an error message to the
	 * user if necessary, and try again later
	 * </p>
	 *
	 * @param lastQuerySuccess the last query success to set
	 */
	public void setLastQuerySuccess(boolean lastQuerySuccess) {
		this.lastQuerySuccess = lastQuerySuccess;
	}

	/**
	 * Get the last query error.
	 * <p>
	 * If lastQuerySuccess was false, this string contains the known error message.
	 * It may be useful for debugging.
	 * </p>
	 *
	 * @return the last query error
	 */
	public String getLastQueryError() {
		return lastQueryError;
	}

	/**
	 * Set the last query error.
	 * <p>
	 * If lastQuerySuccess was false, this string contains the known error message.
	 * It may be useful for debugging.
	 * </p>
	 *
	 * @param lastQueryError the last query error to set
	 */
	public void setLastQueryError(String lastQueryError) {
		this.lastQueryError = lastQueryError;
	}

	/**
	 * @return the database location where state was read from
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the database location where state was read from
	 */
	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public int hashCode() {
		return Objects.hash(state, studentFrom, studentTo, updated, lastQuerySuccess, lastQueryError, source);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof StudentStateInfo)) {
			return false;
		}
		StudentStateInfo o = (StudentStateInfo) obj;
		return Objects.equals(state, o.state) //
				&& Objects.equals(studentFrom, o.studentFrom) //
				&& Objects.equals(studentTo, o.studentTo) //
				&& Objects.equals(updated, o.updated) //
				&& Objects.equals(lastQueryError, o.lastQueryError) //
				&& Objects.equals(lastQuerySuccess, o.lastQuerySuccess) //
				&& Objects.equals(source, o.source);
	}

	@Override
	public String toString() {
		return "StudentStateInfo [state=" + state + ", studentFrom=" + studentFrom + ", studentTo=" + studentTo
				+ ", updated=" + updated + ", lastQuerySuccess=" + lastQuerySuccess + ", lastQueryError="
				+ lastQueryError + ", source=" + source + "]";
	}

}
