package com.trivore.id.sdk.models;

import com.trivore.id.sdk.service.EmailService;

/**
 * An enumeration of the email types. (To, Bcc or Cc)
 * <p>
 * These values are used is {@link EmailService}.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum EmailMode {
	TO, CC, BCC
}
