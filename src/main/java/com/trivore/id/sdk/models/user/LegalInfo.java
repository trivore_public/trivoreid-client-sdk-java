package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trivore.id.sdk.models.user.student.StudentInfo;

/**
 * An object which represents an user's legal info.
 * <p>
 * Used to specify user's legal info in the Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class LegalInfo implements Serializable {

	private String firstNames;
	private String lastName;
	private String callingName;
	private String email;
	private String phone;
	private LegalDomicile domicile;
	private String personalIdentityCode;
	private Boolean protectionOrder;
	private String dateOfBirth;
	private String dateOfDeath;
	private List<Address> addresses;
	private String lastUpdatedAt;
	private StudentInfo studentInfo;

	/**
	 * Construct a new user's legal info.
	 */
	public LegalInfo() {
		// ...
	}

	/**
	 * @return the first names
	 */
	public String getFirstNames() {
		return firstNames;
	}

	/**
	 * @param firstNames the first names to set
	 */
	public void setFirstNames(String firstNames) {
		this.firstNames = firstNames;
	}

	/**
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the last name to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the calling name
	 */
	public String getCallingName() {
		return callingName;
	}

	/**
	 * @param callingName the calling name to set
	 */
	public void setCallingName(String callingName) {
		this.callingName = callingName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the domicile
	 */
	public LegalDomicile getDomicile() {
		return domicile;
	}

	/**
	 * @param domicile the domicile to set
	 */
	public void setDomicile(LegalDomicile domicile) {
		this.domicile = domicile;
	}

	/**
	 * @return the personal identity code
	 */
	public String getPersonalIdentityCode() {
		return personalIdentityCode;
	}

	/**
	 * @param personalIdentityCode the personal identity code to set
	 */
	public void setPersonalIdentityCode(String personalIdentityCode) {
		this.personalIdentityCode = personalIdentityCode;
	}

	/**
	 * @return the protection order
	 */
	public Boolean getProtectionOrder() {
		return protectionOrder;
	}

	/**
	 * @param protectionOrder the protection order to set
	 */
	public void setProtectionOrder(Boolean protectionOrder) {
		this.protectionOrder = protectionOrder;
	}

	/**
	 * @return the date of birth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the date of birth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the date of death
	 */
	public String getDateOfDeath() {
		return dateOfDeath;
	}

	/**
	 * @param dateOfDeath the date of death to set
	 */
	public void setDateOfDeath(String dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}

	/**
	 * @return the addresses
	 */
	public List<Address> getAddresses() {
		return addresses;
	}

	/**
	 * @param addresses the addresses to set
	 */
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	/**
	 * @return the last updated at date
	 */
	public String getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	/**
	 * @param lastUpdatedAt the last updated at date to set
	 */
	public void setLastUpdatedAt(String lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	/**
	 * @return the student info
	 */
	public StudentInfo getStudentInfo() {
		return studentInfo;
	}

	/**
	 * @param studentInfo the student info to set
	 */
	public void setStudentInfo(StudentInfo studentInfo) {
		this.studentInfo = studentInfo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(firstNames, lastName, callingName, email, phone, domicile, personalIdentityCode,
				protectionOrder, dateOfBirth, dateOfDeath, addresses, lastUpdatedAt, studentInfo);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof LegalInfo)) {
			return false;
		}
		LegalInfo o = (LegalInfo) obj;
		return Objects.equals(firstNames, o.firstNames) //
				&& Objects.equals(lastName, o.lastName) //
				&& Objects.equals(callingName, o.callingName) //
				&& Objects.equals(email, o.email) //
				&& Objects.equals(phone, o.phone) //
				&& Objects.equals(domicile, o.domicile) //
				&& Objects.equals(personalIdentityCode, o.personalIdentityCode) //
				&& Objects.equals(protectionOrder, o.protectionOrder) //
				&& Objects.equals(dateOfBirth, o.dateOfBirth) //
				&& Objects.equals(dateOfDeath, o.dateOfDeath) //
				&& Objects.equals(addresses, o.addresses) //
				&& Objects.equals(lastUpdatedAt, o.lastUpdatedAt) //
				&& Objects.equals(studentInfo, o.studentInfo);
	}

	@Override
	public String toString() {
		return "LegalUnfo [firstNames=" + firstNames + ", lastName=" + lastName + ", callingName=" + callingName
				+ ", email=" + email + ", phone=" + phone + ", domicile=" + domicile + ", personalIdentityCode="
				+ personalIdentityCode + ", protectionOrder=" + protectionOrder + ", dateOfBirth=" + dateOfBirth
				+ ", dateOfDeath=" + dateOfDeath + ", addresses=" + addresses + ", lastUpdatedAt=" + lastUpdatedAt
				+ ", studentInfo=" + studentInfo + "]";
	}

}
