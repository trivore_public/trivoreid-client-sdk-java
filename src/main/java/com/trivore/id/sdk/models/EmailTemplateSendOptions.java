package com.trivore.id.sdk.models;


import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"recipients",
		"preferredLocale",
		"preferredTimezone",
		"properties"
})
public class EmailTemplateSendOptions {
	
	@JsonProperty("recipients")
	private List<Recipient> recipients = null;
	@JsonProperty("preferredLocale")
	private String preferredLocale;
	@JsonProperty("preferredTimezone")
	private String preferredTimezone;
	@JsonProperty("properties")
	private Map<String, Object> properties;
	
	@JsonProperty("recipients")
	public List<Recipient> getRecipients() {
		return recipients;
	}
	
	@JsonProperty("recipients")
	public void setRecipients(List<Recipient> recipients) {
		this.recipients = recipients;
	}
	
	@JsonProperty("preferredLocale")
	public String getPreferredLocale() {
		return preferredLocale;
	}
	
	@JsonProperty("preferredLocale")
	public void setPreferredLocale(String preferredLocale) {
		this.preferredLocale = preferredLocale;
	}
	
	@JsonProperty("preferredTimezone")
	public String getPreferredTimezone() {
		return preferredTimezone;
	}
	
	@JsonProperty("preferredTimezone")
	public void setPreferredTimezone(String preferredTimezone) {
		this.preferredTimezone = preferredTimezone;
	}
	
	@JsonProperty("properties")
	public Map<String, Object> getProperties() {
		return properties;
	}
	
	@JsonProperty("properties")
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}
	
}
