package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents MyData Package in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID MyData entries.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ObjectId implements Serializable {

	private String timestamp;
	private String date;
	private int counter;
	private int machineIdentifier;
	private int processIdentifier;
	private int timeSecond;
	private int time;

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the counter
	 */
	public int getCounter() {
		return counter;
	}

	/**
	 * @param counter the counter to set
	 */
	public void setCounter(int counter) {
		this.counter = counter;
	}

	/**
	 * @return the machineIdentifier
	 */
	public int getMachineIdentifier() {
		return machineIdentifier;
	}

	/**
	 * @param machineIdentifier the machineIdentifier to set
	 */
	public void setMachineIdentifier(int machineIdentifier) {
		this.machineIdentifier = machineIdentifier;
	}

	/**
	 * @return the processIdentifier
	 */
	public int getProcessIdentifier() {
		return processIdentifier;
	}

	/**
	 * @param processIdentifier the processIdentifier to set
	 */
	public void setProcessIdentifier(int processIdentifier) {
		this.processIdentifier = processIdentifier;
	}

	/**
	 * @return the timeSecond
	 */
	public int getTimeSecond() {
		return timeSecond;
	}

	/**
	 * @param timeSecond the timeSecond to set
	 */
	public void setTimeSecond(int timeSecond) {
		this.timeSecond = timeSecond;
	}

	/**
	 * @return the time
	 */
	public int getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(int time) {
		this.time = time;
	}

	@Override
	public int hashCode() {
		return Objects.hash(timestamp, date, counter, machineIdentifier, processIdentifier, timeSecond, time);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof ObjectId)) {
			return false;
		}
		ObjectId o = (ObjectId) obj;
		return Objects.equals(timestamp, o.timestamp) //
				&& Objects.equals(date, o.date) //
				&& Objects.equals(counter, o.counter) //
				&& Objects.equals(machineIdentifier, o.machineIdentifier) //
				&& Objects.equals(processIdentifier, o.processIdentifier) //
				&& Objects.equals(timeSecond, o.timeSecond) //
				&& Objects.equals(time, o.time);

	}

	@Override
	public String toString() {
		return "ObjectId [timestamp=" + timestamp + ", date=" + date + ", counter=" + counter + ", machineIdentifier="
				+ machineIdentifier + ", processIdentifier=" + processIdentifier + ", timeSecond=" + timeSecond
				+ ", time=" + time + "]";
	}

}
