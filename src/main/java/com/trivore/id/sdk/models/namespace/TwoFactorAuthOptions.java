package com.trivore.id.sdk.models.namespace;

/**
 * An enumeration of the two factor authentication options.
 * <p>
 * These values are used with {@link GroupPolicy} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum TwoFactorAuthOptions {
	ALLOW_TOTP_AND_SMS_OTL_2FA("Allow TOTP and SMS OTP 2FA"), ALLOW_TOTP_2FA_ONLY("Allow TOTP 2FA only"),
	REQUEST_TOTP_OR_SMS_OTL_2FA("Request TOPT or SMS OTL 2FA"), REQUEST_TOTP_2FA("Request TOTP 2FA");

	private final String value;

	private TwoFactorAuthOptions(String value) {
		this.value = value;
	}

	/**
	 * Get the value of the two factor authentication options.
	 *
	 * @return value of the two factor authentication options
	 */
	public String getValue() {
		return value;
	}

}
