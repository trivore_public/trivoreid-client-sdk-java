package com.trivore.id.sdk.models.namespace;

/**
 * An enumeration of the delete mode.
 * <p>
 * These values are used with {@link GroupPolicy} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum DeleteMode {
	HARD_DELETE, SOFT_DELETE
}
