package com.trivore.id.sdk.models.user.mydata;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents MyData in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID MyData bytes of data.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class MyData implements Serializable {

	private String id;
	private List<String> data;

	/**
	 * Construct a new MyData object.
	 */
	public MyData() {
		// ...
	}

	/**
	 * Get MyData unique identifier.
	 *
	 * @return MyData unique identifier
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set MyData unique identifier.
	 *
	 * @param id MyData unique identifier
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the list with bytes of data.
	 *
	 * @return list with bytes of data
	 */
	public List<String> getData() {
		return data;
	}

	/**
	 * Set the list with bytes of data.
	 *
	 * @param data list with bytes of data
	 */
	public void setData(List<String> data) {
		this.data = data;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof MyData)) {
			return false;
		}
		MyData o = (MyData) obj;
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "MyData [id=" + id + ", data=" + data + "]";
	}

}
