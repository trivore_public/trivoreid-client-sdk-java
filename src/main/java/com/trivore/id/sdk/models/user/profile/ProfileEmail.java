package com.trivore.id.sdk.models.user.profile;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * User profile email.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileEmail implements Serializable {

	private String address;
	private boolean verified;

	/**
	 * Construct Profile Email.
	 */
	public ProfileEmail() {
		// ...
	}

	/**
	 * Construct Profile Email.
	 *
	 * @param address address
	 */
	public ProfileEmail(String address) {
		this.address = address;
	}

	/**
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return verification status
	 */
	public boolean isVerified() {
		return verified;
	}

	/**
	 * @param verified verified
	 */
	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	@Override
	public int hashCode() {
		return Objects.hash(address, verified);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof ProfileEmail)) {
			return false;
		}
		ProfileEmail o = (ProfileEmail) obj;
		return Objects.equals(address, o.address) && Objects.equals(verified, o.verified);
	}

	@Override
	public String toString() {
		return "ProfileEmail [address=" + address + ", verified=" + verified + "]";
	}

}
