package com.trivore.id.sdk.models.user.profile;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * User profile mobile.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileMobile implements Serializable {

	private String number;
	private boolean verified;

	/**
	 * Construct Profile Mobile.
	 */
	public ProfileMobile() {
		// ...
	}

	/**
	 * Construct Profile Mobile.
	 *
	 * @param number number
	 */
	public ProfileMobile(String number) {
		this.number = number;
	}

	/**
	 * @return number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number number
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return verification status
	 */
	public boolean isVerified() {
		return verified;
	}

	/**
	 * @param verified verified
	 */
	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	@Override
	public int hashCode() {
		return Objects.hash(number, verified);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof ProfileMobile)) {
			return false;
		}
		ProfileMobile o = (ProfileMobile) obj;
		return Objects.equals(number, o.number) && Objects.equals(verified, o.verified);
	}

	@Override
	public String toString() {
		return "ProfileMobile [number=" + number + ", verified=" + verified + "]";
	}

}
