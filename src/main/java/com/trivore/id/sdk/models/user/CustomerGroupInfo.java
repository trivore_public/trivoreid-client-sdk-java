package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents a user's customer grouop info.
 * <p>
 * Used to get the user's customer grouop info from Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerGroupInfo implements Serializable {

	private boolean student;
	private String studentIdentifier;
	private String studentValidFrom;
	private String studentValidTo;
	private String studentUpdated;
	private boolean senior;
	private String seniorIdentifier;
	private String seniorUpdated;
	private boolean disabilities;
	private String disabilitiesIdentifier;
	private String disabilitiesUpdated;
	private boolean studentValidNow;

	/**
	 * Get student status.
	 * <p>
	 * Has customer been identified as a student (check validity times separately).
	 * </p>
	 *
	 * @return student status
	 */
	public boolean isStudent() {
		return student;
	}

	/**
	 * Set student status.
	 * <p>
	 * Has customer been identified as a student (check validity times separately).
	 * </p>
	 *
	 * @param student student status
	 */
	public void setStudent(boolean student) {
		this.student = student;
	}

	/**
	 * Get student identifier.
	 * <p>
	 * Who identified customer as a student.
	 * </p>
	 *
	 * @return student identifier
	 */
	public String getStudentIdentifier() {
		return studentIdentifier;
	}

	/**
	 * Set student identifier.
	 * <p>
	 * Who identified customer as a student.
	 * </p>
	 *
	 * @param studentIdentifier student identifier
	 */
	public void setStudentIdentifier(String studentIdentifier) {
		this.studentIdentifier = studentIdentifier;
	}

	/**
	 * Get when does student state start.
	 *
	 * @return when does student state start
	 */
	public String getStudentValidFrom() {
		return studentValidFrom;
	}

	/**
	 * Set when does student state start.
	 *
	 * @param studentValidFrom when does student state start
	 */
	public void setStudentValidFrom(String studentValidFrom) {
		this.studentValidFrom = studentValidFrom;
	}

	/**
	 * Get when does student state end.
	 *
	 * @return when does student state end
	 */
	public String getStudentValidTo() {
		return studentValidTo;
	}

	/**
	 * Set when does student state end.
	 *
	 * @param studentValidTo when does student state end
	 */
	public void setStudentValidTo(String studentValidTo) {
		this.studentValidTo = studentValidTo;
	}

	/**
	 * Get when was student identification last updated.
	 *
	 * @return when was student identification last updated
	 */
	public String getStudentUpdated() {
		return studentUpdated;
	}

	/**
	 * Set when was student identification last updated.
	 *
	 * @param studentUpdated when was student identification last updated
	 */
	public void setStudentUpdated(String studentUpdated) {
		this.studentUpdated = studentUpdated;
	}

	/**
	 * Get senior status.
	 * <p>
	 * Has customer been identified as a senior customer.
	 * </p>
	 *
	 * @return senior status
	 */
	public boolean isSenior() {
		return senior;
	}

	/**
	 * Set senior status.
	 * <p>
	 * Has customer been identified as a senior customer.
	 * </p>
	 *
	 * @param senior senior status
	 */
	public void setSenior(boolean senior) {
		this.senior = senior;
	}

	/**
	 * Get who identified customer as a senior.
	 *
	 * @return who identified customer as a senior
	 */
	public String getSeniorIdentifier() {
		return seniorIdentifier;
	}

	/**
	 * Set who identified customer as a senior.
	 *
	 * @param seniorIdentifier who identified customer as a senior
	 */
	public void setSeniorIdentifier(String seniorIdentifier) {
		this.seniorIdentifier = seniorIdentifier;
	}

	/**
	 * Get when was senior identification last updated.
	 *
	 * @return when was senior identification last updated
	 */
	public String getSeniorUpdated() {
		return seniorUpdated;
	}

	/**
	 * Set when was senior identification last updated.
	 *
	 * @param seniorUpdated when was senior identification last updated
	 */
	public void setSeniorUpdated(String seniorUpdated) {
		this.seniorUpdated = seniorUpdated;
	}

	/**
	 * Get disabilities status.
	 * <p>
	 * Has customer been identified as a disabled customer.
	 * </p>
	 *
	 * @return disabilities status
	 */
	public boolean isDisabilities() {
		return disabilities;
	}

	/**
	 * Set disabilities status.
	 * <p>
	 * Has customer been identified as a disabled customer.
	 * </p>
	 *
	 * @param disabilities disabilities status
	 */
	public void setDisabilities(boolean disabilities) {
		this.disabilities = disabilities;
	}

	/**
	 * Get who identified customer as disabled.
	 *
	 * @return who identified customer as disabled
	 */
	public String getDisabilitiesIdentifier() {
		return disabilitiesIdentifier;
	}

	/**
	 * Set who identified customer as disabled.
	 *
	 * @param disabilitiesIdentifier who identified customer as disabled
	 */
	public void setDisabilitiesIdentifier(String disabilitiesIdentifier) {
		this.disabilitiesIdentifier = disabilitiesIdentifier;
	}

	/**
	 * Get when was disabled identification last updated.
	 *
	 * @return when was disabled identification last updated
	 */
	public String getDisabilitiesUpdated() {
		return disabilitiesUpdated;
	}

	/**
	 * Set when was disabled identification last updated.
	 *
	 * @param disabilitiesUpdated when was disabled identification last updated
	 */
	public void setDisabilitiesUpdated(String disabilitiesUpdated) {
		this.disabilitiesUpdated = disabilitiesUpdated;
	}

	/**
	 * Get is student valid right now.
	 *
	 * @return is student valid right now
	 */
	public boolean isStudentValidNow() {
		return studentValidNow;
	}

	/**
	 * Set is student valid right now.
	 *
	 * @param studentValidNow is student valid right now
	 */
	public void setStudentValidNow(boolean studentValidNow) {
		this.studentValidNow = studentValidNow;
	}

	@Override
	public int hashCode() {
		return Objects.hash(student, studentIdentifier, studentValidFrom, studentValidTo, studentUpdated, senior,
				seniorIdentifier, seniorUpdated, disabilities, disabilitiesIdentifier, disabilitiesUpdated,
				studentValidNow);
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof CustomerGroupInfo)) {
			return false;
		}
		CustomerGroupInfo o = (CustomerGroupInfo) object;
		return Objects.equals(student, o.student) && Objects.equals(studentIdentifier, o.studentIdentifier)
				&& Objects.equals(studentValidFrom, o.studentValidFrom)
				&& Objects.equals(studentValidTo, o.studentValidTo) && Objects.equals(studentUpdated, o.studentUpdated)
				&& Objects.equals(senior, o.senior) && Objects.equals(seniorIdentifier, o.seniorIdentifier)
				&& Objects.equals(seniorUpdated, o.seniorUpdated) && Objects.equals(disabilities, o.disabilities)
				&& Objects.equals(disabilitiesIdentifier, o.disabilitiesIdentifier)
				&& Objects.equals(disabilitiesUpdated, o.disabilitiesUpdated)
				&& Objects.equals(studentValidNow, o.studentValidNow);
	}

	@Override
	public String toString() {
		return "CustomerGroupInfo [student=" + student + ", studentIdentifier=" + studentIdentifier
				+ ", studentValidFrom=" + studentValidFrom + ", studentValidTo=" + studentValidTo + ", studentUpdated="
				+ studentUpdated + ", senior=" + senior + ", seniorIdentifier=" + seniorIdentifier + ", seniorUpdated="
				+ seniorUpdated + ", disabilities=" + disabilities + ", disabilitiesIdentifier="
				+ disabilitiesIdentifier + ", disabilitiesUpdated=" + disabilitiesUpdated + ", studentValidNow="
				+ studentValidNow + "]";
	}

}
