package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.trivore.id.sdk.models.Meta;
import com.trivore.id.sdk.models.user.strong.identification.StrongIdentificationInfo;

/**
 * An object which represents a user with a unique id.
 * <p>
 * Used to get the User information from Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class User implements Serializable {

	private String id;
	private String nsCode;
	private boolean locked;
	private Name name;
	private AccountType userAccountType;
	private Set<Address> addresses;
	private LinkedHashSet<Mobile> mobiles;
	private LinkedHashSet<Email> emails;
	private Set<String> memberOf;
	private String username;
	private String password;
	private Consents consents;
	private String locale;
	private String preferredLanguage;
	private String timeZone;
	private String nickName;
	private String mfaMethod;
	private String websiteMain;
	private String websiteAux;
	private StrongIdentificationInfo strongIdentification;
	private String dateOfBirth;
	private Boolean minor;
	private String domicileCode;
	private List<String> domicileClasses;
	private CustomerGroupInfo customerGroupInfo;
	private Meta meta;
	private String gender;
	private Sex sex;
	private String nationality;
	private String salutation;
	private String title;
	private String birthFirstName;
	private String birthLastName;
	private String birthMiddleName;
	private List<String> tags;

	@JsonProperty("place_of_birth")
	private PlaceOfBirth placeOfBirth;

	/**
	 * Construct a new user object.
	 */
	public User() {
		// ...
	}

	/**
	 * Get user password.
	 *
	 * @return user password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Set user's password.
	 * <p>
	 * Should be used only when creating or modifying user to change password.
	 * </p>
	 *
	 * @param password user's password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Get a unique identifier for the user.
	 * <p>
	 * Every user should have a unique ID. Cannot be empty.
	 * </p>
	 *
	 * @return returns a unique identifier of the user
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set a unique identifier for the user.
	 * <p>
	 * Every user should have a unique ID. Cannot be empty.
	 * </p>
	 *
	 * @param id An unique identifier of the user
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the username.
	 * <p>
	 * Can be an e-mail or the custom username. Must be unique within Namespace.
	 * </p>
	 *
	 * @return returns username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Set the username.
	 * <p>
	 * Can be an e-mail or the custom username. Must be unique within Namespace.
	 * </p>
	 *
	 * @param username username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Get the account type of the user.
	 * <p>
	 * Possible account types:
	 * <ul>
	 * <li>PERSON</li>
	 * <li>LEGAL_ENTITY</li>
	 * </ul>
	 *
	 * @return returns userAccountType
	 */
	public AccountType getUserAccountType() {
		return userAccountType;
	}

	/**
	 * Set the account type of the user.
	 * <p>
	 * Possible account types:
	 * <ul>
	 * <li>PERSON</li>
	 * <li>LEGAL_ENTITY</li>
	 * </ul>
	 *
	 * @param userAccountType userAccountType
	 */
	public void setUserAccountType(AccountType userAccountType) {
		this.userAccountType = userAccountType;
	}

	/**
	 * Get the Namespace code of the user.
	 * <p>
	 * Should be used only when creating user, must be among allowed Namespaces.
	 * </p>
	 *
	 * @return returns namespace code of the user
	 */
	public String getNsCode() {
		return nsCode;
	}

	/**
	 * Set the Namespace code of the user.
	 * <p>
	 * Should be used only when creating user, must be among allowed Namespaces.
	 * </p>
	 *
	 * @param nsCode namespace code of the user
	 */
	public void setNsCode(String nsCode) {
		this.nsCode = nsCode;
	}

	/**
	 * Check if user is locked.
	 *
	 * @return returns User's locked status.
	 */
	public boolean isLocked() {
		return locked;
	}

	/**
	 * Set the user's locked status.
	 *
	 * @param locked User's locked status.
	 */
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	/**
	 * Get the object representing Name in Trivore ID.
	 *
	 * @return Name object
	 */
	public Name getName() {
		if (name == null) {
			name = new Name();
		}
		return name;
	}

	/**
	 * Set the list of objects representing Name in Trivore ID.
	 *
	 * @param name Name object
	 */
	public void setName(Name name) {
		this.name = name;
	}

	/**
	 * Set the list of objects representing Address in Trivore ID.
	 *
	 * @return objects representing Address in Trivore ID.
	 */
	public Set<Address> getAddresses() {
		if (addresses == null) {
			addresses = new HashSet<Address>();
		}
		return addresses;
	}

	/**
	 * Get the list of objects representing Address in Trivore ID.
	 *
	 * @param addresses objects representing Addresses in Trivore ID.
	 */
	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	/**
	 * Get the list of objects representing Mobile in Trivore ID.
	 *
	 * @return objects representing Mobiles in Trivore ID.
	 */
	public LinkedHashSet<Mobile> getMobiles() {
		if (mobiles == null) {
			mobiles = new LinkedHashSet<Mobile>();
		}
		return mobiles;
	}

	/**
	 * Set the list of objects representing Mobile in Trivore ID.
	 *
	 * @param mobiles objects representing Mobiles in Trivore ID.
	 */
	public void setMobiles(LinkedHashSet<Mobile> mobiles) {
		this.mobiles = mobiles;
	}

	/**
	 * Get the list of objects representing Email in Trivore ID.
	 *
	 * @return objects representing Emails in Trivore ID.
	 */
	public LinkedHashSet<Email> getEmails() {
		if (emails == null) {
			emails = new LinkedHashSet<Email>();
		}
		return emails;
	}

	/**
	 * Set the list of objects representing Email in Trivore ID.
	 *
	 * @param emails objects representing Emails in Trivore ID.
	 */
	public void setEmails(LinkedHashSet<Email> emails) {
		this.emails = emails;
	}

	/**
	 * Get the list of user groups ID's.
	 *
	 * @return returns list of group ID's.
	 */
	public Set<String> getMemberOf() {
		if (memberOf == null) {
			memberOf = new HashSet<String>();
		}
		return memberOf;
	}

	/**
	 * Set the list of user groups ID's.
	 *
	 * @param memberOf list of group ID's.
	 */
	public void setMemberOf(Set<String> memberOf) {
		this.memberOf = memberOf;
	}

	/**
	 * Get user consents.
	 *
	 * @return user consents
	 */
	public Consents getConsents() {
		if (consents == null) {
			consents = new Consents();
		}
		return consents;
	}

	/**
	 * Set user consents.
	 *
	 * @param consents user consents
	 */
	public void setConsents(Consents consents) {
		this.consents = consents;
	}

	/**
	 * Get locale.
	 *
	 * @return locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * Set locale.
	 *
	 * @param locale locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * Get preferredLanguage.
	 *
	 * @return preferredLanguage
	 */
	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	/**
	 * Set preferredLanguage.
	 *
	 * @param preferredLanguage preferredLanguage
	 */
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	/**
	 * Get timeZone.
	 *
	 * @return timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * Set timeZone.
	 *
	 * @param timeZone timeZone
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * Get nickName.
	 *
	 * @return nickName
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * Set nickName.
	 *
	 * @param nickName nickName
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * Get mfaMethod.
	 * <p>
	 * TOTP, SMS or NONE.
	 * </p>
	 *
	 * @return mfaMethod
	 */
	public String getMfaMethod() {
		return mfaMethod;
	}

	/**
	 * Set mfaMethod.
	 *
	 * @param mfaMethod mfaMethod
	 */
	public void setMfaMethod(String mfaMethod) {
		this.mfaMethod = mfaMethod;
	}

	/**
	 * Get websiteMain.
	 *
	 * @return websiteMain
	 */
	public String getWebsiteMain() {
		return websiteMain;
	}

	/**
	 * Set websiteMain.
	 *
	 * @param websiteMain websiteMain
	 */
	public void setWebsiteMain(String websiteMain) {
		this.websiteMain = websiteMain;
	}

	/**
	 * Get websiteAux.
	 *
	 * @return websiteAux
	 */
	public String getWebsiteAux() {
		return websiteAux;
	}

	/**
	 * Set websiteAux.
	 *
	 * @param websiteAux websiteAux
	 */
	public void setWebsiteAux(String websiteAux) {
		this.websiteAux = websiteAux;
	}

	/**
	 * Get strongIdentification.
	 *
	 * @return strongIdentification
	 */
	public StrongIdentificationInfo getStrongIdentification() {
		return strongIdentification;
	}

	/**
	 * Set strongIdentification.
	 *
	 * @param strongIdentification strongIdentification
	 */
	public void setStrongIdentification(StrongIdentificationInfo strongIdentification) {
		this.strongIdentification = strongIdentification;
	}

	/**
	 * Get dateOfBirth.
	 *
	 * @return dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * Set dateOfBirth.
	 *
	 * @param dateOfBirth dateOfBirth
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * Get minor.
	 *
	 * @return minor
	 */
	public Boolean isMinor() {
		return minor;
	}

	/**
	 * Set minor.
	 *
	 * @param minor minor
	 */
	public void setMinor(Boolean minor) {
		this.minor = minor;
	}

	/**
	 * Get domicileCode.
	 *
	 * @return domicileCode
	 */
	public String getDomicileCode() {
		return domicileCode;
	}

	/**
	 * Set domicileCode.
	 *
	 * @param domicileCode domicileCode
	 */
	public void setDomicileCode(String domicileCode) {
		this.domicileCode = domicileCode;
	}

	/**
	 * Get customerGroupInfo.
	 *
	 * @return customerGroupInfo
	 */
	public CustomerGroupInfo getCustomerGroupInfo() {
		return customerGroupInfo;
	}

	/**
	 * Set customerGroupInfo.
	 *
	 * @param customerGroupInfo customerGroupInfo
	 */
	public void setCustomerGroupInfo(CustomerGroupInfo customerGroupInfo) {
		this.customerGroupInfo = customerGroupInfo;
	}

	/**
	 * @return meta data
	 */
	public Meta getMeta() {
		return meta;
	}

	/**
	 * @param meta meta data
	 */
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	/**
	 * @return free-form field for gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender free-form field for gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the juridical sex of the user
	 */
	public Sex getSex() {
		return sex;
	}

	/**
	 * @param sex the juridical sex of the user
	 */
	public void setSex(Sex sex) {
		this.sex = sex;
	}

	/**
	 * @return user's nationality in format ISO 3166-1 Alpha-2, e.g. DE
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality user's nationality in format ISO 3166-1 Alpha-2, e.g. DE
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return salutation e.g. "Mr."
	 */
	public String getSalutation() {
		return salutation;
	}

	/**
	 * @param salutation e.g. "Mr."
	 */
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	/**
	 * @return title e.g. "Dr."
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title e.g. "Dr."
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * First name someone has when they are born, or at least from the time they are
	 * a child. This term can be used by a person, who changes the first name later
	 * in life for any reason.
	 *
	 * @return birth first name
	 */
	public String getBirthFirstName() {
		return birthFirstName;
	}

	/**
	 * First name someone has when they are born, or at least from the time they are
	 * a child. This term can be used by a person, who changes the first name later
	 * in life for any reason.
	 *
	 * @param birthFirstName birth first name
	 */
	public void setBirthFirstName(String birthFirstName) {
		this.birthFirstName = birthFirstName;
	}

	/**
	 * Last name someone has when he or she is born, or at least from the time he or
	 * she is a child. This term can be used by a person who changes the family name
	 * later in life for any reason.
	 *
	 * @return birth last name
	 */
	public String getBirthLastName() {
		return birthLastName;
	}

	/**
	 * Last name someone has when he or she is born, or at least from the time he or
	 * she is a child. This term can be used by a person who changes the family name
	 * later in life for any reason.
	 *
	 * @param birthLastName birth last name
	 */
	public void setBirthLastName(String birthLastName) {
		this.birthLastName = birthLastName;
	}

	/**
	 * Middle name someone has when they are born, or at least from the time they
	 * are a child. This term can be used by a person, who changes the middle name
	 * later in life for any reason.
	 *
	 * @return birth middle mame
	 */
	public String getBirthMiddleName() {
		return birthMiddleName;
	}

	/**
	 * Middle name someone has when they are born, or at least from the time they
	 * are a child. This term can be used by a person, who changes the middle name
	 * later in life for any reason.
	 *
	 * @param birthMiddleName birth middle mame
	 */
	public void setBirthMiddleName(String birthMiddleName) {
		this.birthMiddleName = birthMiddleName;
	}

	/**
	 * @return user's place of birth
	 */
	public PlaceOfBirth getPlaceOfBirth() {
		return placeOfBirth;
	}

	/**
	 * @param placeOfBirth user's place of birth
	 */
	public void setPlaceOfBirth(PlaceOfBirth placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	/**
	 * Get municipality classification identifiers
	 *
	 * @return municipality classification identifiers
	 */
	public List<String> getDomicileClasses() {
		if (domicileClasses == null) {
			domicileClasses = new ArrayList<>();
		}
		return domicileClasses;
	}

	/**
	 * Set municipality classification identifiers
	 *
	 * @param domicileClasses municipality classification identifiers
	 */
	public void setDomicileClasses(List<String> domicileClasses) {
		this.domicileClasses = domicileClasses;
	}

	/**
	 * Additional text tags related to user. Tags are automatically converted to
	 * lowercase strings. The maximum number of tags per user is 100 and the maximum
	 * length of a single tag is 100 characters.
	 *
	 * @return additional text tags related to user
	 */
	public List<String> getTags() {
		if (tags == null) {
			tags = new ArrayList<>();
		}
		return tags;
	}

	/**
	 * Additional text tags related to user. Tags are automatically converted to
	 * lowercase strings. The maximum number of tags per user is 100 and the maximum
	 * length of a single tag is 100 characters.
	 *
	 * @param tags additional text tags related to user
	 */
	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof User)) {
			return false;
		}
		User o = (User) object;
		if (o.getId() == null && getId() == null) {
			return super.equals(object);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", nsCode=" + nsCode + ", locked=" + locked + ", name=" + name + ", userAccountType="
				+ userAccountType + ", addresses=" + addresses + ", mobiles=" + mobiles + ", emails=" + emails
				+ ", memberOf=" + memberOf + ", username=" + username + ", password=" + password + ", consents="
				+ consents + ", locale=" + locale + ", preferredLanguage=" + preferredLanguage + ", timeZone="
				+ timeZone + ", nickName=" + nickName + ", mfaMethod=" + mfaMethod + ", websiteMain=" + websiteMain
				+ ", websiteAux=" + websiteAux + ", strongIdentification=" + strongIdentification + ", dateOfBirth="
				+ dateOfBirth + ", minor=" + minor + ", domicileCode=" + domicileCode + ", domicileClasses="
				+ domicileClasses + ", customerGroupInfo=" + customerGroupInfo + ", meta=" + meta + ", gender=" + gender
				+ ", sex=" + sex + ", nationality=" + nationality + ", salutation=" + salutation + ", title=" + title
				+ ", birthFirstName=" + birthFirstName + ", birthLastName=" + birthLastName + ", birthMiddleName="
				+ birthMiddleName + ", tags=" + tags + ", placeOfBirth=" + placeOfBirth + "]";
	}

}
