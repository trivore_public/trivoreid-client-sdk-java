package com.trivore.id.sdk.models.user;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * An object which represents an user's legal domicile.
 * <p>
 * Used to specify user's legal domicile in the Trivore ID service.
 * </p>
 */
public class LegalDomicile {

	private Map<String, String> names;
	private String code;

	/**
	 * Construct legal domicile.
	 */
	public LegalDomicile() {
		// ...
	}

	/**
	 * Get the map of 2-letter language codes to domicile names.
	 *
	 * @return the map of 2-letter language codes to domicile names
	 */
	public Map<String, String> getNames() {
		if (names == null) {
			names = new HashMap<>();
		}
		return names;
	}

	/**
	 * Set the map of 2-letter language codes to domicile names.
	 *
	 * @param names map of 2-letter language codes to domicile names
	 */
	public void setNames(Map<String, String> names) {
		this.names = names;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		return Objects.hash(names, code);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof LegalDomicile)) {
			return false;
		}
		LegalDomicile o = (LegalDomicile) obj;
		return Objects.equals(names, o.names) //
				&& Objects.equals(code, o.code);
	}

	@Override
	public String toString() {
		return "LegalDomicile [names=" + names + ", code=" + code + "]";
	}

}
