package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single enterprise contact info.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EnterpriseContactInfo implements Serializable {

	private String phone;
	private String email;
	private String website;
	private String contacts;

	/**
	 * Get phone number.
	 *
	 * @return phone number
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Set phone number.
	 *
	 * @param phone phone number
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Get email address.
	 *
	 * @return email address
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Set email address.
	 *
	 * @param email email address
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Get the website.
	 *
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * Set the website.
	 *
	 * @param website the website
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * Get contacts.
	 * <p>
	 * Free form contact information. Could be, for example, an address.
	 * </p>
	 *
	 * @return contacts
	 */
	public String getContacts() {
		return contacts;
	}

	/**
	 * Set contacts.
	 * <p>
	 * Free form contact information. Could be, for example, an address.
	 * </p>
	 *
	 * @param contacts contacts
	 */
	public void setContacts(String contacts) {
		this.contacts = contacts;
	}

	@Override
	public int hashCode() {
		return Objects.hash(phone, email, website, contacts);
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof EnterpriseContactInfo)) {
			return false;
		}
		EnterpriseContactInfo o = (EnterpriseContactInfo) object;
		return Objects.equals(phone, o.phone) && Objects.equals(email, o.email) && Objects.equals(website, o.website)
				&& Objects.equals(contacts, o.contacts);
	}

	@Override
	public String toString() {
		return "EnterpriseContactInfo [phone=" + phone + ", email=" + email + ", website=" + website + ", contacts="
				+ contacts + "]";
	}

}
