package com.trivore.id.sdk.models.user.mydata;

/**
 * An enumeration of the backend system.
 * <p>
 * These values are used with {@link MyDataEntry} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum BackendSystem {
	HSLID, TMJ, LIJ_CRM, PUHEET_COM, APSIS, PLUSDIAL, PERINTAKARHU, BONWAL, APJ, VERKKOKAUPPA
}
