package com.trivore.id.sdk.models.user.mydata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents MyData Package in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID MyData entries.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class MyDataPackage implements Serializable {

	private String id;
	private String userId;
	private String collectedOn;
	private String collectingStartedOn;
	private MyDataStatus status;
	private List<MyDataEntry> dataEntries;

	/**
	 * Construct a new MyData package object.
	 */
	public MyDataPackage() {
		// ...
	}

	/**
	 * Get MyData Package unique identifier.
	 *
	 * @return the identifier of the package
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set MyData Package unique identifier.
	 *
	 * @param id the identifier of the package
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get user unique identifier.
	 *
	 * @return user unique identifier
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Set user unique identifier.
	 *
	 * @param userId user unique identifier
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Get status of MyData Package.
	 *
	 * @return status of MyData Package
	 */
	public MyDataStatus getStatus() {
		return status;
	}

	/**
	 * Set status of MyData Package.
	 *
	 * @param status status of MyData Package
	 */
	public void setStatus(MyDataStatus status) {
		this.status = status;
	}

	/**
	 * Get list of data entries.
	 *
	 * @return list of data entries
	 */
	public List<MyDataEntry> getDataEntries() {
		if (dataEntries == null) {
			dataEntries = new ArrayList<>();
		}
		return dataEntries;
	}

	/**
	 * Set list of data entries.
	 *
	 * @param dataEntries list of data entries
	 */
	public void setDataEntries(List<MyDataEntry> dataEntries) {
		this.dataEntries = dataEntries;
	}

	/**
	 * @return collected on
	 */
	public String getCollectedOn() {
		return collectedOn;
	}

	/**
	 * @param collectedOn collectedOn
	 */
	public void setCollectedOn(String collectedOn) {
		this.collectedOn = collectedOn;
	}

	/**
	 * @return collecting started on
	 */
	public String getCollectingStartedOn() {
		return collectingStartedOn;
	}

	/**
	 * @param collectingStartedOn collectingStartedOn
	 */
	public void setCollectingStartedOn(String collectingStartedOn) {
		this.collectingStartedOn = collectingStartedOn;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof MyDataPackage)) {
			return false;
		}
		MyDataPackage o = (MyDataPackage) obj;
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "MyDataPackage [id=" + id + ", userId=" + userId + ", collectedOn=" + collectedOn
				+ ", collectingStartedOn=" + collectingStartedOn + ", status=" + status + ", dataEntries=" + dataEntries
				+ "]";
	}

}
