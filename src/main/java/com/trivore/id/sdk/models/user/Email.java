package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * An object which represents an email in Trivore ID.
 * <p>
 * Used to specify email addresses that can be validated in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Email implements Serializable {

	private String name;
	private String address;
	private List<String> tags;
	private String verifiedBy;
	private String verifiedDateTime;

	@JsonProperty(access = Access.WRITE_ONLY)
	private boolean verified;

	/**
	 * Construct a new email.
	 */
	public Email() {
		// ...
	}

	/**
	 * Construct a new email.
	 *
	 * @param address email address
	 */
	public Email(String address) {
		this.address = address;
	}

	/**
	 * Get email address name.
	 *
	 * @return email address name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set email address name.
	 *
	 * @param name email address name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the actual email address from the email wrapper.
	 *
	 * @return The address from the wrapper.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Set the actual email address for the email wrapper.
	 *
	 * @param address The address for the wrapper.
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Get email tags.
	 *
	 * @return email tags
	 */
	public List<String> getTags() {
		if (tags == null) {
			tags = new ArrayList<>();
		}
		return tags;
	}

	/**
	 * Set email tags.
	 *
	 * @param tags email tags
	 */
	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	/**
	 * Get the read-only definition whether the email is verified.
	 *
	 * @return True if the address is verified and false otherwise.
	 */
	public boolean isVerified() {
		return verified;
	}

	/**
	 * Set the read-only definition whether the email is verified.
	 * <p>
	 * <b>Note:</b> This is a read-only value. Setter is here only to support
	 * Jackson to perform the serialisation.
	 * </p>
	 *
	 * @param verified The definition whether this address is verified.
	 */
	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	/**
	 * @return verified by
	 */
	public String getVerifiedBy() {
		return verifiedBy;
	}

	/**
	 * @param verifiedBy verified by
	 */
	public void setVerifiedBy(String verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	/**
	 * @return verified date time
	 */
	public String getVerifiedDateTime() {
		return verifiedDateTime;
	}

	/**
	 * @param verifiedDateTime verified date time
	 */
	public void setVerifiedDateTime(String verifiedDateTime) {
		this.verifiedDateTime = verifiedDateTime;
	}

	@Override
	public int hashCode() {
		return Objects.hash(address, tags, verified);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Email)) {
			return false;
		}
		Email o = (Email) obj;
		return Objects.equals(verified, o.verified) && Objects.equals(address, o.address);
	}

	@Override
	public String toString() {
		return "Email [name=" + name + ", address=" + address + ", tags=" + tags + ", verified=" + verified + "]";
	}

}
