package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A page wrapper to wrap Trivore ID specific list results.
 * <p>
 * This class is used to wrap Trivore ID list results.
 * </p>
 * <p>
 * An example how to use this class with WebClients.
 * </p>
 *
 * <pre>
 * Page&lt;Group&gt; page = webClient.get().uri("/group").accept(MediaType.APPLICATION_JSON).retrieve()
 * 		.bodyToMono(new ParameterizedTypeReference&lt;Page&lt;Group&gt;&gt;() {
 * 		}).block();
 * </pre>
 *
 * @param <T> The type of the resource being wrapped in the resources list.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Page<T> implements Serializable {

	private Integer totalResults;
	private Integer startIndex;
	private Integer itemsPerPage;
	private List<T> resources;

	/**
	 * Construct a new page object.
	 */
	public Page() {
		// ...
	}

	/**
	 * Get the total amount of results satisfying the provided query.
	 *
	 * @return The total amount of results.
	 */
	public Integer getTotalResults() {
		return totalResults;
	}

	/**
	 * Set the total amount of results satisfying the provided query.
	 *
	 * @param totalResults The total amount of results.
	 */
	public void setTotalResults(Integer totalResults) {
		this.totalResults = totalResults;
	}

	/**
	 * Get the index of the page for the query.
	 *
	 * @return The index of the page.
	 */
	public Integer getStartIndex() {
		return startIndex;
	}

	/**
	 * Set the index of the page.
	 *
	 * @param startIndex The index of the page.
	 */
	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}

	/**
	 * Get the amount of items in a page.
	 *
	 * @return The amount of items in a page.
	 */
	public Integer getItemsPerPage() {
		return itemsPerPage;
	}

	/**
	 * Set the amount of items in a page.
	 *
	 * @param itemsPerPage Amount of items in a page.
	 */
	public void setItemsPerPage(Integer itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	/**
	 * Get the resources attached to this page.
	 *
	 * @return A list of resources attached to this page.
	 */
	public List<T> getResources() {
		if (resources == null) {
			resources = new ArrayList<T>();
		}
		return resources;
	}

	/**
	 * Set the resources attached to this page.
	 *
	 * @param resources A list of resources attached to this page.
	 */
	public void setResources(List<T> resources) {
		this.resources = resources;
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		} else if (!(other instanceof Page)) {
			return false;
		}
		Page<?> o = (Page<?>) other;
		return Objects.equals(totalResults, o.totalResults) && Objects.equals(startIndex, o.startIndex)
				&& Objects.equals(itemsPerPage, o.itemsPerPage) && Objects.equals(resources, o.resources);
	}

	@Override
	public int hashCode() {
		return Objects.hash(totalResults, startIndex, itemsPerPage, resources);
	}

	@Override
	public String toString() {
		return "Page [totalResults=" + totalResults + ", startIndex=" + startIndex + ", itemsPerPage=" + itemsPerPage
				+ ", resources=" + resources + "]";
	}

}
