package com.trivore.id.sdk.models.user.strong.identification;

/**
 * An enumeration of the strong identification method.
 * <p>
 * These values are used with {@link StrongIdentification} objects.
 * </p>
 */
@SuppressWarnings("javadoc")
public enum StrongIdentificationMethod {
	IN_PERSON, SUOMI_FI, SUOMI_FI_VALTUUDET, USER_API
}
