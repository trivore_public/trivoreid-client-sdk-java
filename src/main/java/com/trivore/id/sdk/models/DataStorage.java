package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which presents a single data storage configuration in Trivore ID.
 * <p>
 * Used to control how data storage works at Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class DataStorage implements Serializable {

	private String id;
	private String name;
	private String description;
	private Integer size;
	private String ownerId;

	private Set<String> adminAccess;
	private Set<String> readAccess;
	private Set<String> writeAccess;

	/**
	 * Construct a new data storage object.
	 */
	public DataStorage() {
		// ...
	}

	/**
	 * Get the unique data storage identifier.
	 *
	 * @return An unique identifier of the storage.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set the unique identifier for the data storage.
	 *
	 * @param id An unique identifier for the storage.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the human readable name of the data storage.
	 *
	 * @return The name of the data storage.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set a human readable name for the data storage.
	 *
	 * @param name A name for the data storage.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the additional notes about the data storage.
	 *
	 * @return A description for the data storage.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set the additional notes about the data storage.
	 *
	 * @param description A description for the data storage.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Get the size of data storage in bytes.
	 * <p>
	 * This is a read-only value.
	 * </p>
	 *
	 * @return The size of the data storage in bytes.
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * Set the size for the data storage in bytes.
	 * <p>
	 * This is a read-only value (setter is used in serialisation).
	 * </p>
	 *
	 * @param size The size of the data storage in bytes.
	 */
	public void setSize(Integer size) {
		this.size = size;
	}

	/**
	 * Get the identifier of the owner user/client.
	 * <p>
	 * Storage may be removed together with owner.
	 * </p>
	 *
	 * @return The identifier of the owner user.
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * Set the identifier of the data storage owner user/client.
	 * <p>
	 * Storage may be removed together with owner.
	 * </p>
	 *
	 * @param ownerId The identifier of the owner user.
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * Get the set of user identifiers who can admin the data storage.
	 * <p>
	 * Admins can modify data storage configuration.
	 * </p>
	 *
	 * @return The set of identifiers of admin users.
	 */
	public Set<String> getAdminAccess() {
		return adminAccess;
	}

	/**
	 * Set the set of user identifiers who can admin the data storage.
	 * <p>
	 * Admins can modify data storage configuration.
	 * </p>
	 *
	 * @param adminAccess A set of identifiers of admin users.
	 */
	public void setAdminAccess(Set<String> adminAccess) {
		this.adminAccess = adminAccess;
	}

	/**
	 * Get the set of user identifiers who can read the data storage.
	 * <p>
	 * Readers can read data storage data.
	 * </p>
	 *
	 * @return The set of identifiers of reader users.
	 */
	public Set<String> getReadAccess() {
		return readAccess;
	}

	/**
	 * Set the set of user identifiers who can read the data storage.
	 * <p>
	 * Readers can read data storage data.
	 * </p>
	 *
	 * @param readAccess A set of identifiers of reader users.
	 */
	public void setReadAccess(Set<String> readAccess) {
		this.readAccess = readAccess;
	}

	/**
	 * Get the set of user identifiers who can write into the data storage.
	 * <p>
	 * Writers are allowed write data into the data storage.
	 * </p>
	 *
	 * @return The set of identifiers of writer users.
	 */
	public Set<String> getWriteAccess() {
		return writeAccess;
	}

	/**
	 * Set the set of user identifiers who can write into the data storage.
	 * <p>
	 * Writers are allowed write data into the data storage.
	 * </p>
	 *
	 * @param writeAccess A set of identifiers of writer users.
	 */
	public void setWriteAccess(Set<String> writeAccess) {
		this.writeAccess = writeAccess;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof DataStorage)) {
			return false;
		}
		DataStorage o = (DataStorage) object;
		if (o.getId() == null && getId() == null) {
			return super.equals(object);
		}
		return Objects.equals(id, o.id);
	}

	@Override
	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "DataStorage [id=" + id + ", name=" + name + ", description=" + description + ", size=" + size
				+ ", ownerId=" + ownerId + ", adminAccess=" + adminAccess + ", readAccess=" + readAccess
				+ ", writeAccess=" + writeAccess + "]";
	}
}
