package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trivore.id.sdk.models.user.Email;

/**
 * An object that presents a email message.
 * <p>
 * Used to manage email messages in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailMessage implements Serializable {

	private List<Email> to;
	private List<Email> cc;
	private List<Email> bcc;
	private Email from;
	private List<Email> replyTo;
	private String subject;
	private String html;
	private String text;
	private List<EmailAttachment> attachments;

	/**
	 * Get list of To addresses.
	 *
	 * @return list of To addresses
	 */
	public List<Email> getTo() {
		if (to == null) {
			to = new ArrayList<>();
		}
		return to;
	}

	/**
	 * Set list of To addresses.
	 *
	 * @param to list of To addresses
	 */
	public void setTo(List<Email> to) {
		this.to = to;
	}

	/**
	 * Get list of CC addresses.
	 *
	 * @return list of CC addresses
	 */
	public List<Email> getCc() {
		if (cc == null) {
			cc = new ArrayList<>();
		}
		return cc;
	}

	/**
	 * Set list of CC addresses.
	 *
	 * @param cc list of CC addresses
	 */
	public void setCc(List<Email> cc) {
		this.cc = cc;
	}

	/**
	 * Get list of BCC addresses.
	 *
	 * @return list of BCC addresses
	 */
	public List<Email> getBcc() {
		if (bcc == null) {
			bcc = new ArrayList<>();
		}
		return bcc;
	}

	/**
	 * Set list of BCC addresses.
	 *
	 * @param bcc list of BCC addresses
	 */
	public void setBcc(List<Email> bcc) {
		this.bcc = bcc;
	}

	/**
	 * Get sender email address.
	 *
	 * @return sender email address
	 */
	public Email getFrom() {
		if (from == null) {
			from = new Email();
		}
		return from;
	}

	/**
	 * Set sender email address.
	 *
	 * @param from sender email address
	 */
	public void setFrom(Email from) {
		this.from = from;
	}

	/**
	 * Get reply to email addresses.
	 *
	 * @return reply to email addresses
	 */
	public List<Email> getReplyTo() {
		if (replyTo == null) {
			replyTo = new ArrayList<>();
		}
		return replyTo;
	}

	/**
	 * Set reply to email addresses.
	 *
	 * @param replyTo reply to email addresses
	 */
	public void setReplyTo(List<Email> replyTo) {
		this.replyTo = replyTo;
	}

	/**
	 * Get the subject of the email.
	 *
	 * @return the subject of the email
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Set the subject of the email.
	 *
	 * @param subject the subject of the email
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Get the HTML content.
	 *
	 * @return the HTML content
	 */
	public String getHtml() {
		return html;
	}

	/**
	 * Set the HTML content.
	 *
	 * @param html the HTML content
	 */
	public void setHtml(String html) {
		this.html = html;
	}

	/**
	 * Get the email test body.
	 *
	 * @return the email test body
	 */
	public String getText() {
		return text;
	}

	/**
	 * Set the email test body.
	 *
	 * @param text the email test body
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * Get the email attachments.
	 *
	 * @return the email attachments
	 */
	public List<EmailAttachment> getAttachments() {
		if (attachments == null) {
			attachments = new ArrayList<>();
		}
		return attachments;
	}

	/**
	 * Set the email attachments.
	 *
	 * @param attachments the email attachments
	 */
	public void setAttachments(List<EmailAttachment> attachments) {
		this.attachments = attachments;
	}

	@Override
	public int hashCode() {
		return Objects.hash(to, cc, bcc, from, replyTo, subject, text, attachments, html);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof EmailMessage)) {
			return false;
		}
		EmailMessage o = (EmailMessage) obj;
		return Objects.equals(to, o.to) && Objects.equals(cc, o.cc) && Objects.equals(bcc, o.bcc)
				&& Objects.equals(from, o.from) && Objects.equals(replyTo, o.replyTo)
				&& Objects.equals(subject, o.subject) && Objects.equals(attachments, o.attachments)
				&& Objects.equals(html, o.html) && Objects.equals(text, o.text);
	}

	@Override
	public String toString() {
		return "EmailMessage [to=" + to + ", cc=" + cc + ", bcc=" + bcc + ", from=" + from + ", replyTo=" + replyTo
				+ ", subject=" + subject + ", html=" + html + ", text=" + text + ", attachments=" + attachments + "]";
	}

}
