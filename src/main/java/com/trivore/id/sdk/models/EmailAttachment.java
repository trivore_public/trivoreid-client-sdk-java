package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.Objects;

public class EmailAttachment implements Serializable {
	
	private  String name;
	private  String data;
	private  String type;
	private  String disposition;
	private  String contentId;
	
	/**
	 * Gets the name of the attachment. The name will be used as the file name in the email message.
	 * @return attachment file name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name of the attachment. The name will be used as the file name in the email message.
	 * @param name attachment file name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the data for the attachment as a base64 encoded string.
	 * @return a base64 encoded string
	 */
	public String getData() {
		return data;
	}
	
	/**
	 * Sets the data for the attachment as a base64 encoded string.
	 * @param data a base64 encoded string
	 */
	public void setData(String data) {
		this.data = data;
	}
	
	/**
	 * Gets the MIME type of the attachment. E.g. "image/jpeg"
	 * @return attachment MIME type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Sets the MIME type of the attachment. E.g. "image/jpeg"
	 * @param type attachment MIME type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Gets the disposition of the attachment. Accepted values are "inline" and "attachment". Defaults to "attachment".
	 * @return attachment disposition
	 */
	public String getDisposition() {
		return disposition;
	}
	
	/**
	 * Sets the disposition of the attachment. Accepted values are "inline" and "attachment". Defaults to "attachment".
	 * @param disposition attachment disposition
	 */
	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}
	
	/**
	 * Gets the content id to be used in the email HTML content when referring to the attachment.
	 * @return attachment content id
	 */
	public String getContentId() {
		return contentId;
	}
	
	/**
	 * Sets the content id to be used in the email HTML content when referring to the attachment.
	 * @param contentId attachment content id
	 */
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof EmailAttachment)) {
			return false;
		}
		EmailAttachment that = (EmailAttachment) o;
		return Objects.equals(name, that.name) &&
				Objects.equals(data, that.data) &&
				Objects.equals(type, that.type) &&
				Objects.equals(disposition, that.disposition) &&
				Objects.equals(contentId, that.contentId);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(name, data, type, disposition, contentId);
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("EmailAttachment{");
		sb.append("name='").append(name).append('\'');
		sb.append(", data='").append(data).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", disposition='").append(disposition).append('\'');
		sb.append(", contentId='").append(contentId).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
