package com.trivore.id.sdk.models;

import java.io.Serializable;

/**
 * Class representing Meta data.
 */
@SuppressWarnings("serial")
public class Meta implements Serializable {

	private String created;
	private String lastModified;
	private String location;

	/**
	 * @return time when resource was created
	 */
	public String getCreated() {
		return created;
	}

	/**
	 * @param created time when resource was created
	 */
	public void setCreated(String created) {
		this.created = created;
	}

	/**
	 * @return time when resource was last modified
	 */
	public String getLastModified() {
		return lastModified;
	}

	/**
	 * @param lastModified time when resource was last modified
	 */
	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return resource's location URI
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location resource's location URI
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((created == null) ? 0 : created.hashCode());
		result = prime * result + ((lastModified == null) ? 0 : lastModified.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Meta other = (Meta) obj;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (lastModified == null) {
			if (other.lastModified != null)
				return false;
		} else if (!lastModified.equals(other.lastModified))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Meta [created=" + created + ", lastModified=" + lastModified + ", location=" + location + "]";
	}

}
