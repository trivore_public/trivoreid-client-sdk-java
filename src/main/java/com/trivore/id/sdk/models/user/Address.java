package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents an address.
 * <p>
 * Used to specify addresses in the Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address implements Serializable {

	private String country;
	private String locality;
	private String postalCode;
	private String region;
	private String streetAddress;
	private AddressType type;
	private String organisation;
	private String addressName;
	private String name;
	private boolean verified;
	private String source;
	private String language;
	private String from;
	private String until;
	private List<String> tags;
	
	
	/**
	 * Construct a new address.
	 */
	public Address() {
		// ...
	}

	/**
	 * Get user's country.
	 *
	 * @return returns country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Set user's country.
	 *
	 * @param country country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Get user's locality or city.
	 *
	 * @return returns locality or city
	 */
	public String getLocality() {
		return locality;
	}

	/**
	 * Set user's locality or city.
	 *
	 * @param locality locality or city
	 */
	public void setLocality(String locality) {
		this.locality = locality;
	}

	/**
	 * Get user's postal code or zip code.
	 *
	 * @return returns postal code or zip code
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * Set user's postal code or zip code.
	 *
	 * @param postalCode postal code or zip code
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Get user's region or state.
	 *
	 * @return returns region or state
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * Set user's region or state.
	 *
	 * @param region region or state
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * Get user's street address.
	 *
	 * @return returns street address
	 */
	public String getStreetAddress() {
		return streetAddress;
	}

	/**
	 * Set user's street address.
	 *
	 * @param streetAddress street address
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	/**
	 * @return the type
	 */
	public AddressType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(AddressType type) {
		this.type = type;
	}

	/**
	 * @return the organisation
	 */
	public String getOrganisation() {
		return organisation;
	}

	/**
	 * @param organisation the organisation to set
	 */
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	/**
	 * @return the addressName
	 */
	public String getAddressName() {
		return addressName;
	}

	/**
	 * @param addressName the addressName to set
	 */
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the verified
	 */
	public boolean isVerified() {
		return verified;
	}

	/**
	 * @param verified the verified to set
	 */
	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	
	/**
	 * Gets the datetime from which the address is valid
	 *
	 * @return a datetime string in ISO 8601 format
	 */
	public String getFrom() {
		return from;
	}
	
	/**
	 * Sets the datetime from which the address is valid
	 *
	 * @param from a datetime string in ISO 8601 format
	 */
	public void setFrom(String from) {
		this.from = from;
	}
	
	/**
	 * Gets the datetime until the address is valid
	 *
	 * @return datetime string in ISO 8601 format
	 */
	public String getUntil() {
		return until;
	}
	
	/**
	 * Sets the datetime until the address is valid
	 *
	 * @param until a datetime string in ISO 8601 format
	 */
	public void setUntil(String until) {
		this.until = until;
	}
	
	/**
	 * Gets the address tags
	 *
	 * @return Address tags, never null
	 */
	public List<String> getTags() {
		if (tags == null) {
			tags = new ArrayList<>();
		}
		return tags;
	}
	
	/**
	 * Set the address tags
	 * @param tags a list of tags
	 */
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	/**
	 * Helper to check if all Address fields are empty/null or not.
	 *
	 * @return if all Address fields are empty/null
	 */
	public boolean areFieldsEmpty() {
		return (country == null || country.isEmpty()) && (locality == null || locality.isEmpty())
				&& (postalCode == null || postalCode.isEmpty()) && (region == null || region.isEmpty())
				&& (streetAddress == null || streetAddress.isEmpty());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(country, locality, postalCode, region, streetAddress, type, organisation, addressName, name,
				verified, source, language, from, until, tags);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Address)) {
			return false;
		}
		Address o = (Address) obj;
		return Objects.equals(country, o.country) //
				&& Objects.equals(locality, o.locality) //
				&& Objects.equals(postalCode, o.postalCode) //
				&& Objects.equals(region, o.region) //
				&& Objects.equals(streetAddress, o.streetAddress) //
				&& Objects.equals(type, o.type) //
				&& Objects.equals(organisation, o.organisation) //
				&& Objects.equals(addressName, o.addressName) //
				&& Objects.equals(name, o.name) //
				&& Objects.equals(verified, o.verified) //
				&& Objects.equals(source, o.source) //
				&& Objects.equals(language, o.language)
				&& Objects.equals(from, o.from)
				&& Objects.equals(until, o.until)
				&& Objects.equals(tags, o.tags);
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Address{");
		sb.append("country='").append(country).append('\'');
		sb.append(", locality='").append(locality).append('\'');
		sb.append(", postalCode='").append(postalCode).append('\'');
		sb.append(", region='").append(region).append('\'');
		sb.append(", streetAddress='").append(streetAddress).append('\'');
		sb.append(", type=").append(type);
		sb.append(", organisation='").append(organisation).append('\'');
		sb.append(", addressName='").append(addressName).append('\'');
		sb.append(", name='").append(name).append('\'');
		sb.append(", verified=").append(verified);
		sb.append(", source='").append(source).append('\'');
		sb.append(", language='").append(language).append('\'');
		sb.append(", from='").append(from).append('\'');
		sb.append(", until='").append(until).append('\'');
		sb.append(", tags=").append(tags);
		sb.append('}');
		return sb.toString();
	}
}
