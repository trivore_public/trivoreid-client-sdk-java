package com.trivore.id.sdk.models;


import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"emailAddress",
		"userId",
		"preferredLocale",
		"preferredTimezone",
		"properties"
})
public class Recipient {
	
	@JsonProperty("emailAddress")
	private String emailAddress;
	@JsonProperty("userId")
	private String userId;
	@JsonProperty("preferredLocale")
	private String preferredLocale;
	@JsonProperty("preferredTimezone")
	private String preferredTimezone;
	@JsonProperty("properties")
	private Map<String, Object> properties;
	
	@JsonProperty("emailAddress")
	public String getEmailAddress() {
		return emailAddress;
	}
	
	@JsonProperty("emailAddress")
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	@JsonProperty("userId")
	public String getUserId() {
		return userId;
	}
	
	@JsonProperty("userId")
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@JsonProperty("preferredLocale")
	public String getPreferredLocale() {
		return preferredLocale;
	}
	
	@JsonProperty("preferredLocale")
	public void setPreferredLocale(String preferredLocale) {
		this.preferredLocale = preferredLocale;
	}
	
	@JsonProperty("preferredTimezone")
	public String getPreferredTimezone() {
		return preferredTimezone;
	}
	
	@JsonProperty("preferredTimezone")
	public void setPreferredTimezone(String preferredTimezone) {
		this.preferredTimezone = preferredTimezone;
	}
	
	@JsonProperty("properties")
	public Map<String, Object> getProperties() {
		return properties;
	}
	
	@JsonProperty("properties")
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}
	
}