package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * User's place of birth.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlaceOfBirth implements Serializable {

	private String country;
	private String locality;
	private String region;

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the locality
	 */
	public String getLocality() {
		return locality;
	}

	/**
	 * @param locality the locality to set
	 */
	public void setLocality(String locality) {
		this.locality = locality;
	}

	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * @param region the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public int hashCode() {
		return Objects.hash(country, locality, region);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof PlaceOfBirth)) {
			return false;
		}
		PlaceOfBirth o = (PlaceOfBirth) obj;
		return Objects.equals(country, o.country) //
				&& Objects.equals(locality, o.locality) //
				&& Objects.equals(region, o.region);
	}

	@Override
	public String toString() {
		return "PlaceOfBirth [country=" + country + ", locality=" + locality + ", region=" + region + "]";
	}

}
