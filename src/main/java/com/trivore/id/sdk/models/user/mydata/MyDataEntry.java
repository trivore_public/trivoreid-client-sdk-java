package com.trivore.id.sdk.models.user.mydata;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trivore.id.sdk.models.ObjectId;

/**
 * An object which presents MyData Entry in Trivore ID.
 * <p>
 * This object is used to wrap and map Trivore ID MyData Entry.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class MyDataEntry implements Serializable {

	private String code;
	private String backendSystemName;
	private MyData data;
	private MyDataStatus status;
	private String mimeType;
	private String account;
	private String dataId;
	private String updatedOn;
	private Map<String, Object> document;
	private ObjectId dataIdAsObjectId;
	private BackendSystem backendSystem;

	/**
	 * Construct a new MyData entry object.
	 */
	public MyDataEntry() {
		// ...
	}

	/**
	 * Get MyData entry code identifier.
	 *
	 * @return entry code identifier
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Set MyData entry code identifier.
	 *
	 * @param code entry code identifier
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Get backend system name.
	 *
	 * @return backend system name
	 */
	public String getBackendSystemName() {
		return backendSystemName;
	}

	/**
	 * Set backend system name.
	 *
	 * @param backendSystemName backend system name
	 */
	public void setBackendSystemName(String backendSystemName) {
		this.backendSystemName = backendSystemName;
	}

	/**
	 * Get the object representing MyData in Trivore ID.
	 *
	 * @return object representing MyData
	 */
	public MyData getData() {
		if (data == null) {
			data = new MyData();
		}
		return data;
	}

	/**
	 * Set the object representing MyData in Trivore ID.
	 *
	 * @param data object representing MyData
	 */
	public void setData(MyData data) {
		this.data = data;
	}

	/**
	 * Get the status of MyData entry.
	 *
	 * @return status of MyData entry
	 */
	public MyDataStatus getStatus() {
		return status;
	}

	/**
	 * Set the status of MyData entry.
	 *
	 * @param status status of MyData entry
	 */
	public void setStatus(MyDataStatus status) {
		this.status = status;
	}

	/**
	 * Get the data unique identifier.
	 * <p>
	 * This value is also used as the target value to perform REST API calls.
	 * </p>
	 *
	 * @return data unique identifier
	 */
	public String getDataId() {
		return dataId;
	}

	/**
	 * Set the data unique identifier.
	 * <p>
	 * This value is also used as the target value to perform REST API calls.
	 * </p>
	 *
	 * @param dataId data unique identifier
	 */
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	/**
	 * @return mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @param mimeType mimeType
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * @return account
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @param account account
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @return updatedOn
	 */
	public String getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn updatedOn
	 */
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return document
	 */
	public Map<String, Object> getDocument() {
		return document;
	}

	/**
	 * @param document document
	 */
	public void setDocument(Map<String, Object> document) {
		this.document = document;
	}

	/**
	 * @return dataIdAsObjectId
	 */
	public ObjectId getDataIdAsObjectId() {
		return dataIdAsObjectId;
	}

	/**
	 * @param dataIdAsObjectId dataIdAsObjectId
	 */
	public void setDataIdAsObjectId(ObjectId dataIdAsObjectId) {
		this.dataIdAsObjectId = dataIdAsObjectId;
	}

	/**
	 * @return backendSystem
	 */
	public BackendSystem getBackendSystem() {
		return backendSystem;
	}

	/**
	 * @param backendSystem backendSystem
	 */
	public void setBackendSystem(BackendSystem backendSystem) {
		this.backendSystem = backendSystem;
	}

	@Override
	public int hashCode() {
		return Objects.hash(dataId);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof MyDataEntry)) {
			return false;
		}
		MyDataEntry o = (MyDataEntry) obj;
		return Objects.equals(dataId, o.dataId);
	}

	@Override
	public String toString() {
		return "MyDataEntry [code=" + code + ", backendSystemName=" + backendSystemName + ", data=" + data + ", status="
				+ status + ", dataId=" + dataId + "]";
	}

}
