package com.trivore.id.sdk.models.user.profile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trivore.id.sdk.models.user.Address;
import com.trivore.id.sdk.models.user.Consents;
import com.trivore.id.sdk.models.user.Name;

/**
 * An object which represents a user's profile.
 * <p>
 * Used to specify user's profile in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Profile implements Serializable {

	private List<Address> addresses;
	private List<Address> legalAddresses;
	private List<ProfileEmail> emails;
	private List<ProfileMobile> mobiles;
	private Name name;
	private String nickName;
	private String dateOfBirth;
	private boolean minor;
	private String domicileCode;
	private String legalDomicileCode;
	private Consents consents;
	private String lastModified;
	private String timeZone;
	private String locale;

	/**
	 * Get list of user addresses.
	 *
	 * @return list of user addresses
	 */
	public List<Address> getAddresses() {
		if (addresses == null) {
			addresses = new ArrayList<>();
		}
		return addresses;
	}

	/**
	 * Set list of user addresses.
	 *
	 * @param addresses list of user addresses
	 */
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	/**
	 * Get list of user legal addresses from the legal authority. Readonly.
	 *
	 * @return list of user legal addresses from the legal authority
	 */
	public List<Address> getLegalAddresses() {
		if (legalAddresses == null) {
			legalAddresses = new ArrayList<>();
		}
		return legalAddresses;
	}

	/**
	 * Get list of user emails.
	 *
	 * @return list of user emails
	 */
	public List<ProfileEmail> getEmails() {
		if (emails == null) {
			emails = new ArrayList<>();
		}
		return emails;
	}

	/**
	 * Set list of user emails.
	 *
	 * @param emails list of user emails
	 */
	public void setEmails(List<ProfileEmail> emails) {
		this.emails = emails;
	}

	/**
	 * Get list of user mobiles.
	 *
	 * @return list of user mobiles
	 */
	public List<ProfileMobile> getMobiles() {
		if (mobiles == null) {
			mobiles = new ArrayList<>();
		}
		return mobiles;
	}

	/**
	 * Set list of user mobiles.
	 *
	 * @param mobiles list of user mobiles
	 */
	public void setMobiles(List<ProfileMobile> mobiles) {
		this.mobiles = mobiles;
	}

	/**
	 * Get the user name.
	 *
	 * @return user name
	 */
	public Name getName() {
		if (name == null) {
			name = new Name();
		}
		return name;
	}

	/**
	 * Set the user name.
	 *
	 * @param name user name
	 */
	public void setName(Name name) {
		this.name = name;
	}

	/**
	 * Get the user nickname.
	 *
	 * @return user nickname
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * Set the user nickname.
	 *
	 * @param nickName user nickname
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * Get the user date of birth.
	 * <p>
	 * Example: 1900-12-29
	 * </p>
	 *
	 * @return date of birth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * Set the user date of birth.
	 * <p>
	 * Example: 1900-12-29
	 * </p>
	 *
	 * @param dateOfBirth date of birth
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * Get if the user is minor.
	 * <p>
	 * Is user a legal minor (client defined age). Can be used as alternative to
	 * dateOfBirth.
	 * </p>
	 *
	 * @return if the user is minor
	 */
	public boolean isMinor() {
		return minor;
	}

	/**
	 * Set if the user is minor.
	 * <p>
	 * Is user a legal minor (client defined age). Can be used as alternative to
	 * dateOfBirth.
	 * </p>
	 *
	 * @param minor if the user is minor
	 */
	public void setMinor(boolean minor) {
		this.minor = minor;
	}

	/**
	 * Get the user domicile code (kuntanumero).
	 *
	 * @return domicile code (kuntanumero)
	 */
	public String getDomicileCode() {
		return domicileCode;
	}

	/**
	 * Set the user domicile code (kuntanumero).
	 *
	 * @param domicileCode domicile code (kuntanumero)
	 */
	public void setDomicileCode(String domicileCode) {
		this.domicileCode = domicileCode;
	}

	/**
	 * Get the user legal domicile code (kuntanumero) from the legal authority.
	 * Readonly.
	 *
	 * @return legal domicile code (kuntanumero) from the legal authority
	 */
	public String getLegalDomicileCode() {
		return legalDomicileCode;
	}

	/**
	 * Get the user concents.
	 *
	 * @return user concents
	 */
	public Consents getConsents() {
		if (consents == null) {
			consents = new Consents();
		}
		return consents;
	}

	/**
	 * @return lastModified
	 */
	public String getLastModified() {
		return lastModified;
	}

	/**
	 * @param lastModified lastModified
	 */
	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * @return timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone timeZone
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * @return locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @param legalAddresses legalAddresses
	 */
	public void setLegalAddresses(List<Address> legalAddresses) {
		this.legalAddresses = legalAddresses;
	}

	/**
	 * @param legalDomicileCode legalDomicileCode
	 */
	public void setLegalDomicileCode(String legalDomicileCode) {
		this.legalDomicileCode = legalDomicileCode;
	}

	/**
	 * Set the user user concents.
	 *
	 * @param consents user concents
	 */
	public void setConsents(Consents consents) {
		this.consents = consents;
	}

	@Override
	public int hashCode() {
		return Objects.hash(addresses, dateOfBirth, domicileCode, emails, legalAddresses, legalDomicileCode, consents,
				minor, mobiles, name, nickName);

	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Profile)) {
			return false;
		}
		Profile o = (Profile) obj;
		return Objects.equals(addresses, o.addresses) && Objects.equals(dateOfBirth, o.dateOfBirth)
				&& Objects.equals(domicileCode, o.domicileCode) && Objects.equals(emails, o.emails)
				&& Objects.equals(legalAddresses, o.legalAddresses)
				&& Objects.equals(legalDomicileCode, o.legalDomicileCode) && Objects.equals(consents, o.consents)
				&& Objects.equals(minor, o.minor) && Objects.equals(mobiles, o.mobiles) && Objects.equals(name, o.name)
				&& Objects.equals(nickName, o.nickName);
	}

	@Override
	public String toString() {
		return "Profile [addresses=" + addresses + ", legalAddresses=" + legalAddresses + ", emails=" + emails
				+ ", mobiles=" + mobiles + ", name=" + name + ", nickName=" + nickName + ", dateOfBirth=" + dateOfBirth
				+ ", minor=" + minor + ", domicileCode=" + domicileCode + ", legalDomicileCode=" + legalDomicileCode
				+ ", consents=" + consents + "]";
	}

}
