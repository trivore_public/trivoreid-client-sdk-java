package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Class representing SMS response from Trivore ID server.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SMSResponse implements Serializable {

	private String status;
	private String description;
	private String action;
	private String messageId;
	private String to;
	private String toRegion;
	private String clientRef;
	private String billingRef;
	private String carrierRef;
	private int messageCount;
	private String billingState;
	private String totalPrice;
	private String remainingCredits;

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the messageId
	 */
	public String getMessageId() {
		return messageId;
	}

	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return the toRegion
	 */
	public String getToRegion() {
		return toRegion;
	}

	/**
	 * @param toRegion the toRegion to set
	 */
	public void setToRegion(String toRegion) {
		this.toRegion = toRegion;
	}

	/**
	 * @return the clientRef
	 */
	public String getClientRef() {
		return clientRef;
	}

	/**
	 * @param clientRef the clientRef to set
	 */
	public void setClientRef(String clientRef) {
		this.clientRef = clientRef;
	}

	/**
	 * @return the billingRef
	 */
	public String getBillingRef() {
		return billingRef;
	}

	/**
	 * @param billingRef the billingRef to set
	 */
	public void setBillingRef(String billingRef) {
		this.billingRef = billingRef;
	}

	/**
	 * @return the carrierRef
	 */
	public String getCarrierRef() {
		return carrierRef;
	}

	/**
	 * @param carrierRef the carrierRef to set
	 */
	public void setCarrierRef(String carrierRef) {
		this.carrierRef = carrierRef;
	}

	/**
	 * @return the messageCount
	 */
	public int getMessageCount() {
		return messageCount;
	}

	/**
	 * @param messageCount the messageCount to set
	 */
	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}

	/**
	 * @return the billingState
	 */
	public String getBillingState() {
		return billingState;
	}

	/**
	 * @param billingState the billingState to set
	 */
	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	/**
	 * @return the totalPrice
	 */
	public String getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the remainingCredits
	 */
	public String getRemainingCredits() {
		return remainingCredits;
	}

	/**
	 * @param remainingCredits the remainingCredits to set
	 */
	public void setRemainingCredits(String remainingCredits) {
		this.remainingCredits = remainingCredits;
	}

	@Override
	public int hashCode() {
		if (getMessageId() == null) {
			return super.hashCode();
		}
		return Objects.hash(messageId);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof SMSResponse)) {
			return false;
		}
		SMSResponse o = (SMSResponse) obj;
		if (o.getMessageId() == null && getMessageId() == null) {
			return super.equals(obj);
		}
		return Objects.equals(messageId, o.messageId);
	}

	@Override
	public String toString() {
		return "SMSResponse [status=" + status + ", description=" + description + ", action=" + action + ", messageId="
				+ messageId + ", to=" + to + ", toRegion=" + toRegion + ", clientRef=" + clientRef + ", billingRef="
				+ billingRef + ", carrierRef=" + carrierRef + ", messageCount=" + messageCount + ", billingState="
				+ billingState + ", totalPrice=" + totalPrice + ", remainingCredits=" + remainingCredits
				+ ", getStatus()=" + getStatus() + ", getDescription()=" + getDescription() + ", getAction()="
				+ getAction() + ", getMessageId()=" + getMessageId() + ", getTo()=" + getTo() + ", getToRegion()="
				+ getToRegion() + ", getClientRef()=" + getClientRef() + ", getBillingRef()=" + getBillingRef()
				+ ", getCarrierRef()=" + getCarrierRef() + ", getMessageCount()=" + getMessageCount()
				+ ", getBillingState()=" + getBillingState() + ", getTotalPrice()=" + getTotalPrice()
				+ ", getRemainingCredits()=" + getRemainingCredits() + ", hashCode()=" + hashCode() + ", getClass()="
				+ getClass() + ", toString()=" + super.toString() + "]";
	}

}
