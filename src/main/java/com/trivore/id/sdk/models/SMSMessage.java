package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object that presents an SMS message.
 * <p>
 * Used to manage SMS messages in Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SMSMessage implements Serializable {

	private String to;
	private String toName;
	private String toRegion;
	private String from;
	private boolean fromRequired;
	private int messageClass;
	private String text;
	private String data;
	private String udh;
	private int ttl;
	private String clientRef;
	private String billingRef;
	private String carrierRef;
	private String callback;

	/**
	 * Get the message recipient mobile phone number.
	 *
	 * @return message recipient mobile phone number
	 */
	public String getTo() {
		return to;
	}

	/**
	 * Set the message recipient mobile phone number.
	 * <p>
	 * Must be valid phone number (E.164, international " + "or national format
	 * within the namespace's default region).
	 * </p>
	 *
	 * @param to message recipient mobile phone number
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * Get the message recipient display name. Used mostly for user-interface
	 * purposes.
	 *
	 * @return message recipient display name
	 */
	public String getToName() {
		return toName;
	}

	/**
	 * Set the message recipient display name. Used mostly for user-interface
	 * purposes.
	 *
	 * @param toName message recipient display name
	 */
	public void setToName(String toName) {
		this.toName = toName;
	}

	/**
	 * Get the message recipient default region (country or subdivision).
	 *
	 * @return message recipient default region (country or subdivision)
	 */
	public String getToRegion() {
		return toRegion;
	}

	/**
	 * Set the message recipient default region (country or subdivision).
	 * <p>
	 * This affects number parsing only if number is given in national format. If
	 * this parameter is missing, namespace's default region is used instead. Must
	 * be either two-letter ISO 3166-1 country code or two-letter ISO 3166-2
	 * subdivision code.
	 * </p>
	 *
	 * @param toRegion message recipient default region (country or subdivision)
	 */
	public void setToRegion(String toRegion) {
		this.toRegion = toRegion;
	}

	/**
	 * Get the message sender number or alphanumeric name.
	 *
	 * @return message sender number or alphanumeric name
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * Set the message sender number or alphanumeric name.
	 * <p>
	 * Accepted characters are a-z, A-Z and 0-9. Maximum length is 11 characters.
	 * </p>
	 *
	 * @param from sender number or alphanumeric name
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * Get message sender either required or optional.
	 *
	 * @return is message sender required
	 */
	public boolean isFromRequired() {
		return fromRequired;
	}

	/**
	 * Set message sender either required or optional.
	 * <p>
	 * If message sender (from) is set but this parameter is false, message may be
	 * sent via gateway that does not support sender address.
	 * </p>
	 *
	 * @param fromRequired is message sender required
	 */
	public void setFromRequired(boolean fromRequired) {
		this.fromRequired = fromRequired;
	}

	/**
	 * Get the SMS message class, valid values = 0 (flash message), 1, 2, 3
	 *
	 * @return SMS message class
	 */
	public int getMessageClass() {
		return messageClass;
	}

	/**
	 * Set the SMS message class, valid values = 0 (flash message), 1, 2, 3
	 *
	 * @param messageClass SMS message class
	 */
	public void setMessageClass(int messageClass) {
		this.messageClass = messageClass;
	}

	/**
	 * Get the message textual content.
	 *
	 * @return message textual content
	 */
	public String getText() {
		return text;
	}

	/**
	 * Set the message textual content.
	 *
	 * @param text message textual content
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Get the message binary content, hex encoded.
	 *
	 * @return message binary content, hex encoded
	 */
	public String getData() {
		return data;
	}

	/**
	 * Set the message binary content, hex encoded.
	 *
	 * @param data message binary content, hex encoded
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * Get the message User Data Header, hex encoded.
	 *
	 * @return message User Data Header, hex encoded
	 */
	public String getUdh() {
		return udh;
	}

	/**
	 * Set the message User Data Header, hex encoded.
	 *
	 * @param udh message User Data Header, hex encoded
	 */
	public void setUdh(String udh) {
		this.udh = udh;
	}

	/**
	 * Get the SMS time to live (validity) in minutes.
	 *
	 * @return SMS time to live (validity) in minutes.
	 */
	public int getTtl() {
		return ttl;
	}

	/**
	 * Set the SMS time to live (validity) in minutes.
	 *
	 * @param ttl SMS time to live (validity) in minutes.
	 */
	public void setTtl(int ttl) {
		this.ttl = ttl;
	}

	/**
	 * Get the free-form, unique client reference for this message.
	 *
	 * @return free-form, unique client reference for this message
	 */
	public String getClientRef() {
		return clientRef;
	}

	/**
	 * Set the free-form, unique client reference for this message.
	 * <p>
	 * This is included as part of delivery reports. Maximum length is 40
	 * characters.
	 * </p>
	 *
	 * @param clientRef free-form, unique client reference for this message
	 */
	public void setClientRef(String clientRef) {
		this.clientRef = clientRef;
	}

	/**
	 * Get the free-form billing reference for this message.
	 *
	 * @return free-form billing reference for this message
	 */
	public String getBillingRef() {
		return billingRef;
	}

	/**
	 * Set the free-form billing reference for this message.
	 * <p>
	 * Do NOT use this as unique id but rather as billing category, such as 'My
	 * campaign' or 'Fire alarm'. Maximum length is 40 characters.
	 * </p>
	 *
	 * @param billingRef free-form billing reference for this message
	 */
	public void setBillingRef(String billingRef) {
		this.billingRef = billingRef;
	}

	/**
	 * Get the free-form carrier reference for this message.
	 *
	 * @return free-form carrier reference for this message
	 */
	public String getCarrierRef() {
		return carrierRef;
	}

	/**
	 * Set the free-form carrier reference for this message.
	 * <p>
	 * This is mostly used for internal purposes to prevent message routing loops.
	 * You may use this parameter to identify the machine sending the message, such
	 * as 'myserver.example.com'.
	 * </p>
	 *
	 * @param carrierRef free-form carrier reference for this message
	 */
	public void setCarrierRef(String carrierRef) {
		this.carrierRef = carrierRef;
	}

	/**
	 * Get the delivery report callback URL.
	 *
	 * @return delivery report callback URL
	 */
	public String getCallback() {
		return callback;
	}

	/**
	 * Set the delivery report callback URL.
	 * <p>
	 * Specify this parameter if you want to receive message delivery reports. This
	 * URL must not require any authentication, must accept application/json POST
	 * data and must return HTTP status code 200 on successful operation
	 * </p>
	 *
	 * @param callback delivery report callback URL
	 */
	public void setCallback(String callback) {
		this.callback = callback;
	}

	@Override
	public int hashCode() {
		return Objects.hash(to, toName, toRegion, from, fromRequired, messageClass, text, data, udh, ttl, clientRef,
				billingRef, carrierRef, callback);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof SMSMessage)) {
			return false;
		}
		SMSMessage o = (SMSMessage) obj;
		return Objects.equals(to, o.to) && Objects.equals(toName, o.toName) && Objects.equals(toRegion, o.toRegion)
				&& Objects.equals(from, o.from) && Objects.equals(text, o.text) && Objects.equals(data, o.data);
	}

	@Override
	public String toString() {
		return "SMSMessage [to=" + to + ", toName=" + toName + ", toRegion=" + toRegion + ", from=" + from
				+ ", fromRequired=" + fromRequired + ", messageClass=" + messageClass + ", text=" + text + ", data="
				+ data + ", udh=" + udh + ", ttl=" + ttl + ", clientRef=" + clientRef + ", billingRef=" + billingRef
				+ ", carrierRef=" + carrierRef + ", callback=" + callback + "]";
	}

}
