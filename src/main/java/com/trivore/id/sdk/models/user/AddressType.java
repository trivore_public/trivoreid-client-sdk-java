package com.trivore.id.sdk.models.user;

/**
 * Enum defining the {@link Address} type.
 */
@SuppressWarnings("javadoc")
public enum AddressType {
	PERMANENT, TEMPORARY, POSTAL
}
