package com.trivore.id.sdk.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object that presents a region in Trivore ID.
 * <p>
 * Used to get list of regions from the Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Region implements Serializable {

	private String regionCode;
	private String name;
	private int callingCode;
	private List<String> nationalCodes;
	private String flagURL;

	/**
	 * Build a new region.
	 */
	public Region() {
		// ...
	}

	/**
	 * Get the region code.
	 *
	 * @return the region code
	 */
	public String getRegionCode() {
		return regionCode;
	}

	/**
	 * Set the the region code.
	 *
	 * @param regionCode the region code
	 */
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	/**
	 * Get the region name.
	 *
	 * @return region name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the region name.
	 *
	 * @param name region name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the calling code.
	 *
	 * @return calling code
	 */
	public int getCallingCode() {
		return callingCode;
	}

	/**
	 * Set the calling code.
	 *
	 * @param callingCode calling code
	 */
	public void setCallingCode(int callingCode) {
		this.callingCode = callingCode;
	}

	/**
	 * Get the list of national codes.
	 *
	 * @return list of national codes
	 */
	public List<String> getNationalCodes() {
		if (nationalCodes == null) {
			nationalCodes = new ArrayList<>();
		}
		return nationalCodes;
	}

	/**
	 * Set the list of national codes.
	 *
	 * @param nationalCodes list of national codes
	 */
	public void setNationalCodes(List<String> nationalCodes) {
		this.nationalCodes = nationalCodes;
	}

	/**
	 * Get the flag URL.
	 *
	 * @return flag URL
	 */
	public String getFlagURL() {
		return flagURL;
	}

	/**
	 * Set the flag URL.
	 *
	 * @param flagURL flag URL
	 */
	public void setFlagURL(String flagURL) {
		this.flagURL = flagURL;
	}

	@Override
	public int hashCode() {
		return Objects.hash(regionCode, name, callingCode, nationalCodes, flagURL);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof Region)) {
			return false;
		}
		Region o = (Region) obj;
		return Objects.equals(regionCode, o.regionCode) && Objects.equals(name, o.name)
				&& Objects.equals(callingCode, o.callingCode) && Objects.equals(nationalCodes, o.nationalCodes)
				&& Objects.equals(flagURL, o.flagURL);
	}

	@Override
	public String toString() {
		return "Region [regionCode=" + regionCode + ", name=" + name + ", callingCode=" + callingCode
				+ ", nationalCodes=" + nationalCodes + ", flagURL=" + flagURL + "]";
	}

}
