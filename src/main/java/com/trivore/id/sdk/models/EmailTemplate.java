package com.trivore.id.sdk.models;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"id",
		"name",
		"description",
		"templateEngine",
		"defaultLocale",
		"defaultTimeZone",
		"subjectTemplate",
		"htmlTemplate",
		"textTemplate",
		"localeProperties",
		"namespaceIds"
})
public class EmailTemplate {
	
	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("templateEngine")
	private String templateEngine;
	@JsonProperty("defaultLocale")
	private String defaultLocale;
	@JsonProperty("defaultTimeZone")
	private String defaultTimeZone;
	@JsonProperty("subjectTemplate")
	private String subjectTemplate;
	@JsonProperty("htmlTemplate")
	private String htmlTemplate;
	@JsonProperty("textTemplate")
	private String textTemplate;
	@JsonProperty("namespaceIds")
	private List<String> namespaceIds = null;
	@JsonProperty("localeProperties")
	private Map<String, Map<String, String>> localeProperties;
	
	
	@JsonProperty("id")
	public String getId() {
		return id;
	}
	
	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}
	
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}
	
	@JsonProperty("templateEngine")
	public String getTemplateEngine() {
		return templateEngine;
	}
	
	@JsonProperty("templateEngine")
	public void setTemplateEngine(String templateEngine) {
		this.templateEngine = templateEngine;
	}
	
	@JsonProperty("defaultLocale")
	public String getDefaultLocale() {
		return defaultLocale;
	}
	
	@JsonProperty("defaultLocale")
	public void setDefaultLocale(String defaultLocale) {
		this.defaultLocale = defaultLocale;
	}
	
	@JsonProperty("defaultTimeZone")
	public String getDefaultTimeZone() {
		return defaultTimeZone;
	}
	
	@JsonProperty("defaultTimeZone")
	public void setDefaultTimeZone(String defaultTimeZone) {
		this.defaultTimeZone = defaultTimeZone;
	}
	
	@JsonProperty("subjectTemplate")
	public String getSubjectTemplate() {
		return subjectTemplate;
	}
	
	@JsonProperty("subjectTemplate")
	public void setSubjectTemplate(String subjectTemplate) {
		this.subjectTemplate = subjectTemplate;
	}
	
	@JsonProperty("htmlTemplate")
	public String getHtmlTemplate() {
		return htmlTemplate;
	}
	
	@JsonProperty("htmlTemplate")
	public void setHtmlTemplate(String htmlTemplate) {
		this.htmlTemplate = htmlTemplate;
	}
	
	@JsonProperty("textTemplate")
	public String getTextTemplate() {
		return textTemplate;
	}
	
	@JsonProperty("textTemplate")
	public void setTextTemplate(String textTemplate) {
		this.textTemplate = textTemplate;
	}
	
	@JsonProperty("localeProperties")
	public Map<String, Map<String, String>> getLocaleProperties() {
		return localeProperties;
	}
	
	@JsonProperty("localeProperties")
	public void setLocaleProperties(Map<String, Map<String, String>> localeProperties) {
		this.localeProperties = localeProperties;
	}
	
	@JsonProperty("namespaceIds")
	public List<String> getNamespaceIds() {
		return namespaceIds;
	}
	
	@JsonProperty("namespaceIds")
	public void setNamespaceIds(List<String> namespaceIds) {
		this.namespaceIds = namespaceIds;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof EmailTemplate)) {
			return false;
		}
		EmailTemplate that = (EmailTemplate) o;
		return Objects.equals(id, that.id) &&
				Objects.equals(name, that.name) &&
				Objects.equals(description, that.description) &&
				Objects.equals(templateEngine, that.templateEngine) &&
				Objects.equals(defaultLocale, that.defaultLocale) &&
				Objects.equals(defaultTimeZone, that.defaultTimeZone) &&
				Objects.equals(subjectTemplate, that.subjectTemplate) &&
				Objects.equals(htmlTemplate, that.htmlTemplate) &&
				Objects.equals(textTemplate, that.textTemplate) &&
				Objects.equals(namespaceIds, that.namespaceIds) &&
				Objects.equals(localeProperties, that.localeProperties);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id, name, description, templateEngine, defaultLocale, defaultTimeZone, subjectTemplate,
				htmlTemplate, textTemplate, namespaceIds, localeProperties);
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("EmailTemplate{");
		sb.append("id='").append(id).append('\'');
		sb.append(", name='").append(name).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append(", templateEngine='").append(templateEngine).append('\'');
		sb.append(", defaultLocale='").append(defaultLocale).append('\'');
		sb.append(", defaultTimeZone='").append(defaultTimeZone).append('\'');
		sb.append(", subjectTemplate='").append(subjectTemplate).append('\'');
		sb.append(", htmlTemplate='").append(htmlTemplate).append('\'');
		sb.append(", textTemplate='").append(textTemplate).append('\'');
		sb.append(", namespaceIds=").append(namespaceIds);
		sb.append(", localeProperties=").append(localeProperties);
		sb.append('}');
		return sb.toString();
	}
}

