package com.trivore.id.sdk.models.user;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * An object which represents a password validation / complexity requirement
 * information.
 * <p>
 * Used to specify the password validation / complexity requirement information
 * in the Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordRequirements implements Serializable {

	private int minLength;
	private int maxLength;
	private int numberOfCharacteristics;
	private int alphabeticalSequenceMaxLength;
	private int numericalSequenceMaxLength;
	private int qwertySequenceMaxLength;
	private int historyLength;
	private int repeatCharacterLength;
	private boolean usernameForbidden;
	private boolean dictionaryCheck;

	/**
	 * Get the minimum password length.
	 *
	 * @return minimum password length
	 */
	public int getMinLength() {
		return minLength;
	}

	/**
	 * Set the minimum password length.
	 *
	 * @param minLength minimum password length
	 */
	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	/**
	 * Get the maximum password length.
	 *
	 * @return maximum password length
	 */
	public int getMaxLength() {
		return maxLength;
	}

	/**
	 * Set the maximum password length.
	 *
	 * @param maxLength maximum password length
	 */
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	/**
	 * Get if username is forbiddein in password.
	 *
	 * @return if username is forbiddein in password
	 */
	public boolean isUsernameForbidden() {
		return usernameForbidden;
	}

	/**
	 * Set if username is forbiddein in password.
	 *
	 * @param usernameForbidden if username is forbiddein in password
	 */
	public void setUsernameForbidden(boolean usernameForbidden) {
		this.usernameForbidden = usernameForbidden;
	}

	/**
	 * @return number of characteristics
	 */
	public int getNumberOfCharacteristics() {
		return numberOfCharacteristics;
	}

	/**
	 * @param numberOfCharacteristics number of characteristics
	 */
	public void setNumberOfCharacteristics(int numberOfCharacteristics) {
		this.numberOfCharacteristics = numberOfCharacteristics;
	}

	/**
	 * @return alphabetical sequence max length
	 */
	public int getAlphabeticalSequenceMaxLength() {
		return alphabeticalSequenceMaxLength;
	}

	/**
	 * @param alphabeticalSequenceMaxLength alphabetical sequence max length
	 */
	public void setAlphabeticalSequenceMaxLength(int alphabeticalSequenceMaxLength) {
		this.alphabeticalSequenceMaxLength = alphabeticalSequenceMaxLength;
	}

	/**
	 * @return numerical sequence max length
	 */
	public int getNumericalSequenceMaxLength() {
		return numericalSequenceMaxLength;
	}

	/**
	 * @param numericalSequenceMaxLength numerical sequence max length
	 */
	public void setNumericalSequenceMaxLength(int numericalSequenceMaxLength) {
		this.numericalSequenceMaxLength = numericalSequenceMaxLength;
	}

	/**
	 * @return qwerty sequence max length
	 */
	public int getQwertySequenceMaxLength() {
		return qwertySequenceMaxLength;
	}

	/**
	 * @param qwertySequenceMaxLength qwerty sequence max length
	 */
	public void setQwertySequenceMaxLength(int qwertySequenceMaxLength) {
		this.qwertySequenceMaxLength = qwertySequenceMaxLength;
	}

	/**
	 * @return history length
	 */
	public int getHistoryLength() {
		return historyLength;
	}

	/**
	 * @param historyLength history length
	 */
	public void setHistoryLength(int historyLength) {
		this.historyLength = historyLength;
	}

	/**
	 * @return repeat character length
	 */
	public int getRepeatCharacterLength() {
		return repeatCharacterLength;
	}

	/**
	 * @param repeatCharacterLength repeat character length
	 */
	public void setRepeatCharacterLength(int repeatCharacterLength) {
		this.repeatCharacterLength = repeatCharacterLength;
	}

	/**
	 * @return dictionary check
	 */
	public boolean isDictionaryCheck() {
		return dictionaryCheck;
	}

	/**
	 * @param dictionaryCheck dictionarycheck
	 */
	public void setDictionaryCheck(boolean dictionaryCheck) {
		this.dictionaryCheck = dictionaryCheck;
	}

	@Override
	public int hashCode() {
		return Objects.hash(minLength, maxLength, numberOfCharacteristics, alphabeticalSequenceMaxLength,
				numericalSequenceMaxLength, qwertySequenceMaxLength, historyLength, repeatCharacterLength,
				usernameForbidden, dictionaryCheck);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof PasswordRequirements)) {
			return false;
		}
		PasswordRequirements o = (PasswordRequirements) obj;
		return Objects.equals(minLength, o.minLength)
				&& Objects.equals(numberOfCharacteristics, o.numberOfCharacteristics)
				&& Objects.equals(maxLength, o.maxLength)
				&& Objects.equals(alphabeticalSequenceMaxLength, o.alphabeticalSequenceMaxLength)
				&& Objects.equals(numericalSequenceMaxLength, o.numericalSequenceMaxLength)
				&& Objects.equals(qwertySequenceMaxLength, o.qwertySequenceMaxLength)
				&& Objects.equals(historyLength, o.historyLength)
				&& Objects.equals(repeatCharacterLength, o.repeatCharacterLength)
				&& Objects.equals(usernameForbidden, o.usernameForbidden)
				&& Objects.equals(dictionaryCheck, o.dictionaryCheck);
	}

	@Override
	public String toString() {
		return "PasswordComplexity [minLength=" + minLength + ", maxLength=" + maxLength + ", numberOfCharacteristics="
				+ numberOfCharacteristics + ", alphabeticalSequenceMaxLength=" + alphabeticalSequenceMaxLength
				+ ", numericalSequenceMaxLength=" + numericalSequenceMaxLength + ", qwertySequenceMaxLength="
				+ qwertySequenceMaxLength + ", historyLength=" + historyLength + ", repeatCharacterLength="
				+ repeatCharacterLength + ", usernameForbidden=" + usernameForbidden + ", dictionaryCheck="
				+ dictionaryCheck + "]";
	}

}
