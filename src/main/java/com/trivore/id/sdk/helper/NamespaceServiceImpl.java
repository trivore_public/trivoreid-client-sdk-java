package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.namespace.Namespace;
import com.trivore.id.sdk.service.NamespaceService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * Namespace Service helper to perform namespace related requests to TrivoreID.
 */
public class NamespaceServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final NamespaceService service;

	/**
	 * Construct Namespace Service helper.
	 *
	 * @param service namespace service
	 */
	public NamespaceServiceImpl(NamespaceService service) {
		this.service = service;
	}

	/**
	 * Get all namespaces satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with namespace objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Namespace> getAll() throws TrivoreIDException, IOException {
		return getAll(new Criteria());
	}

	/**
	 * Get all namespaces satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with namespace objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<Namespace> getAll(Criteria criteria) throws TrivoreIDException, IOException {
		Assert.notNull(criteria, "The criteria cannot be null");

		String filter = "";
		if (criteria.getFilter() != null) {
			filter = criteria.getFilter().toFilterString();
		}

		Response<Page<Namespace>> response = service.getAll(criteria.getStartIndex(), criteria.getCount(), filter)
				.execute();
		if (response.isSuccessful()) {
			Page<Namespace> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} namespaces with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find namespaces with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Add a new namespace into the service.
	 *
	 * @param namespace The namespace to be added.
	 * @return A namespace saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Namespace create(Namespace namespace) throws IOException, TrivoreIDException {
		Assert.notNull(namespace, "The namespace cannot be null");

		Response<Namespace> response = service.create(namespace).execute();

		if (response.isSuccessful()) {
			Namespace newNamespace = response.body();
			log.info("Successfully created namespace with id {}", newNamespace.getId());
			return newNamespace;
		} else {
			log.error("Failed to create a namespace.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the namespace with the given identifier.
	 *
	 * @param namespaceId The id of the target namespace.
	 * @return The namespace with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Namespace get(String namespaceId) throws IOException, TrivoreIDException {
		Assert.notNull(namespaceId, "The id cannot be null");
		Assert.hasText(namespaceId, "The id cannot be empty");

		Response<Namespace> response = service.get(namespaceId).execute();

		if (response.isSuccessful()) {
			Namespace namespace = response.body();
			log.info("Successfully got the namespace with id {}", namespaceId);
			return namespace;
		} else {
			log.error("Failed to get the namespace with id {}", namespaceId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Save the changes of the provided namespace.
	 * <p>
	 * Requests TrivoreID to save (i.e. update) the provided namespace.
	 * </p>
	 *
	 * @param namespace   The target namespace.
	 * @return A namespace saved in TrivoreID.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Namespace update(Namespace namespace) throws IOException, TrivoreIDException {
		Assert.notNull(namespace, "The namespace cannot be null");
		Assert.notNull(namespace.getId(), "The ID cannot be null");
		Assert.hasText(namespace.getId(), "The ID cannot be empty");

		Response<Namespace> response = service.update(namespace.getId(), namespace).execute();

		if (response.isSuccessful()) {
			Namespace updatedNamespace = response.body();
			log.info("Successfully modified the namespace with ID {}.", updatedNamespace.getId());
			return updatedNamespace;
		} else {
			log.error("Failed to update the namespace with id {}.", namespace.getId());
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Delete a namespace from TrivoreID.
	 *
	 * @param namespaceId The id of the target namespace.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public void delete(String namespaceId) throws IOException, TrivoreIDException {
		Assert.notNull(namespaceId, "The namespaceId cannot be null");
		Assert.hasText(namespaceId, "The namespaceId cannot be empty");

		Response<Void> response = service.delete(namespaceId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully deleted namespace with id {}.", namespaceId);
		} else {
			log.error("Failed to delete namespace with id {}.", namespaceId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}
}
