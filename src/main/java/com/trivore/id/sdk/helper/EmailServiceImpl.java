package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.EmailMessage;
import com.trivore.id.sdk.service.EmailService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * A group service for all kinds of email messaging operations.
 * <p>
 * Handles all kinds of operations with TrivoreID email messages.
 * </p>
 */
public class EmailServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private EmailService service;

	/**
	 * Construct Namespace Service helper.
	 *
	 * @param service namespace service
	 */
	public EmailServiceImpl(EmailService service) {
		this.service = service;
	}

	/**
	 * Send a custom email message.
	 *
	 * @param message email message
	 * @throws TrivoreIDException In case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void send(EmailMessage message) throws IOException, TrivoreIDException {
		Assert.notNull(message, "The message cannot be null");

		Response<Void> response = service.send(message).execute();
		if (response.isSuccessful()) {
			log.info("Successfully sent email message.");
		} else {
			log.error("Failed to send email message.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Send a custom email message to a user.
	 *
	 * @param message email message
	 * @param userId  target user id
	 * @throws TrivoreIDException In case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void sendToUser(EmailMessage message, String userId) throws IOException, TrivoreIDException {
		Assert.notNull(message, "The message cannot be null");
		Assert.notNull(userId, "The userId cannot be null");
		Assert.hasText(userId, "The userId cannot be empty");

		Response<Void> response = service.sendToUser(userId, message).execute();
		if (response.isSuccessful()) {
			log.info("Successfully sent email message to a user {}.", userId);
		} else {
			log.error("Failed to send email message to a user {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Send email message to all group members.
	 *
	 * @param message  email message to send
	 * @param groupIDs target group IDs
	 * @throws TrivoreIDException In case of a network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public void sendToGroupMembers(EmailMessage message, String... groupIDs) throws IOException, TrivoreIDException {
		Assert.notNull(message, "The message cannot be null");
		Assert.notNull(groupIDs, "The groups cannot be null");

		for (String id : groupIDs) {
			Response<Void> response = service.sendToGroupMembers(id, message).execute();
			if (response.isSuccessful()) {
				log.info("Successfully sent email message to a group {}.", id);
			} else {
				log.error("Failed to send email message to a group {}.", id);
				throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
			}
		}
	}

}
