package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.EmailTemplate;
import com.trivore.id.sdk.models.EmailTemplateSendOptions;
import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.service.EmailTemplateService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.Criteria;
import com.trivore.id.sdk.utils.ExceptionsUtil;
import com.trivore.id.sdk.utils.Filter;

import retrofit2.Response;

/**
 * Service for retrieving email templates as well as sending emails based on email templates
 */
public class EmailTemplateServiceImpl  {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private EmailTemplateService emailTemplateService;
	
	/**
	 * Construct email template service helper
	 * @param emailTemplateService the underlying service to delegate requests to
	 */
	public EmailTemplateServiceImpl(EmailTemplateService emailTemplateService) {
		this.emailTemplateService = emailTemplateService;
	}
	
	
	/**
	 * Get all email templates satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @return page with {@link EmailTemplate} objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<EmailTemplate> getAll() throws IOException, TrivoreIDException {
		return getAll(new Criteria());
	}
	
	/**
	 * Get all email templates satisfying the provided {@link Criteria}.
	 * <p>
	 * <b>Note:</b> The maximum page size is 500 items.
	 * </p>
	 *
	 * @param criteria the query criteria for the operation.
	 * @return page with {@link EmailTemplate} objects
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public Page<EmailTemplate> getAll(Criteria criteria) throws IOException, TrivoreIDException {
		Assert.notNull(criteria, "The criteria cannot be null");
		
		String filterString = Optional.ofNullable(criteria.getFilter()).map(Filter::toString).orElse("");
		
		Response<Page<EmailTemplate>> response = emailTemplateService.getAll(criteria.getStartIndex(), criteria.getCount(), filterString).execute();
		if (response.isSuccessful()) {
			Page<EmailTemplate> page = response.body();
			int count = page == null ? 0 : page.getResources().size();
			log.info("Successfully found {} email templates with criteria {}", count, criteria);
			return page;
		} else {
			log.error("Failed to find email templates with criteria {}", criteria);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
		
	}
	
	/**
	 * Get the email template with the given identifier.
	 *
	 * @param emailTemplateId The id of the email template.
	 * @return The email template with the given identifier.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public EmailTemplate get(String emailTemplateId) throws IOException, TrivoreIDException {
		Assert.notNull(emailTemplateId, "The id cannot be null");
		Assert.hasText(emailTemplateId, "The id cannot be empty");
		
		Response<EmailTemplate> response = emailTemplateService.get(emailTemplateId).execute();
		
		if (response.isSuccessful()) {
			EmailTemplate template = response.body();
			log.info("Successfully got the email template with id {}", emailTemplateId);
			return template;
		} else {
			log.error("Failed to get the email template with id {}", emailTemplateId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}
	
	/**
	 * Sends an email message based on the template with the given templateId
	 * @param templateId the id of the email template to use
	 * @param sendOptions options regarding sending of the email template message, including the recipient email addresses
	 */
	public void sendTemplateEmail(String templateId, EmailTemplateSendOptions sendOptions) throws TrivoreIDException, IOException {
		Assert.notNull(templateId, "The id cannot be null");
		Assert.hasText(templateId, "The id cannot be empty");
		
		Response<Void> response = emailTemplateService.send(templateId, sendOptions).execute();
		if (response.isSuccessful()) {
			log.info("Successfully sent email template message with id {}", templateId);
		} else {
			log.error("Failed to send email template message with id {}", templateId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
		
	}

}
