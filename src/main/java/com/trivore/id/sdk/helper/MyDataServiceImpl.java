package com.trivore.id.sdk.helper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.user.mydata.MyDataPackage;
import com.trivore.id.sdk.service.MyDataService;
import com.trivore.id.sdk.utils.Assert;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import retrofit2.Response;

/**
 * A MyData service for all kinds of MyData operations.
 * <p>
 * Handles all kinds of operations with TrivoreID MyData objects.
 * </p>
 */
public class MyDataServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final MyDataService service;

	/**
	 * Construct MyData Service helper.
	 *
	 * @param service namespace service
	 */
	public MyDataServiceImpl(MyDataService service) {
		this.service = service;
	}

	/**
	 * Request a creation of a MyData package for the user with target id.
	 * <p>
	 * Function will send a MyData package creation request to TrivoreID.
	 * </p>
	 *
	 * @param userId The id of the target user.
	 * @return A package containing all MyData entries for the user.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public MyDataPackage request(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null.");
		Assert.hasText(userId, "The userId cannot be empty.");

		Response<MyDataPackage> response = service.request(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully requested MyData for the user with id {}", userId);
			return response.body();
		} else {
			log.error("Failed to request MyData for the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get all MyData entries for the target user.
	 * <p>
	 * Function queries all MyData entries for the user from the TrivoreID.
	 * </p>
	 *
	 * @param userId The id of the target user.
	 * @return A package containing all MyData entries for the user.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public MyDataPackage getAll(String userId) throws TrivoreIDException, IOException {
		Assert.notNull(userId, "The userId cannot be null.");
		Assert.hasText(userId, "The userId cannot be empty.");

		Response<MyDataPackage> response = service.getAll(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got MyData package for the user with id {}", userId);
			return response.body();
		} else {
			log.error("Failed to get MyData package with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get a single MyData file for the target user.
	 * <p>
	 * Returns a single file from the MyData package. Requires access to user's
	 * Namespace.
	 * </p>
	 *
	 * @param userId The id of the target user.
	 * @param dataId The id of the target MyData entry.
	 * @return A byte array containing the target MyData entry.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Map<String, Object> getMyData(String userId, String dataId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null.");
		Assert.hasText(userId, "The userId cannot be empty.");
		Assert.notNull(dataId, "The dataId cannot be null.");
		Assert.hasText(dataId, "The dataId cannot be empty.");

		Response<Map<String, Object>> response = service.getMyData(userId, dataId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got MyData with id {} for the user with id {}", dataId, userId);
			return response.body();
		} else {
			log.error("Failed to get MyData entry with id {} and user id {}.", dataId, userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get a user info of the target user.
	 * <p>
	 * Returns user information in unstable JSON format. Format may change. Contents
	 * may be censored. If specific user information is required, other APIs should
	 * be used.
	 * </p>
	 *
	 * @param userId The id of the target user.
	 * @return A map containing user info.
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public Map<String, Object> getUserInfo(String userId) throws IOException, TrivoreIDException {
		Assert.notNull(userId, "The userId cannot be null.");
		Assert.hasText(userId, "The userId cannot be empty.");

		Response<Map<String, Object>> response = service.getUserInfo(userId).execute();

		if (response.isSuccessful()) {
			log.info("Successfully got personal data of the user with id {}", userId);
			return response.body();
		} else {
			log.error("Failed to get personal data of the user with id {}.", userId);
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

}
