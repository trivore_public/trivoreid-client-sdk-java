package com.trivore.id.sdk.utils;

import static java.util.Objects.requireNonNull;

/**
 * An enumeration of all supported filters.
 * <p>
 * These values are used with {@link Filter} objects.
 * </p>
 */
public enum FilterType {

	/** The attribute and operator values must match. */
	EQUAL("eq"),
	/** The attribute and operator values must not match. */
	NOT_EQUAL("ne"),
	/** The attribute value must contain the entire operator value. */
	CONTAINS("co"),
	/** The entire operator value must begin with the attribute value. */
	STARTS_WITH("sw"),
	/** The entire operator value must end with the attribute value. */
	ENDS_WITH("ew"),
	/** The attribute must have a non-empty and non-null value. */
	PRESENT("pr"),
	/** The attribute value must be greater than operator value. */
	GREATER_THAN("gt"),
	/** The attribute value must be greater or equal than operator value. */
	GREATER_OR_EQUAL_THAN("ge"),
	/** The attribute value must be less than operator value. */
	LESS_THAN("lt"),
	/** The attribute value must be less or equal than operator value. */
	LESS_OR_EQUAL_THAN("le"),
	/** Both expressions must match. */
	AND("and"),
	/** Either expressions must match. */
	OR("or");

	private final String operator;

	/**
	 * Construct a new filter type with the provided operator.
	 *
	 * @param operator The operator (a short name for the type).
	 * @throws NullPointerException Whether the operator is null.
	 */
	FilterType(String operator) throws NullPointerException {
		this.operator = requireNonNull(operator);
	}

	/**
	 * Get the operator of the filter type.
	 * <p>
	 * This value is the one that's being used in the request URI.
	 * </p>
	 *
	 * @return The operator of the filter type.
	 */
	public String getOperator() {
		return operator;
	}

}
