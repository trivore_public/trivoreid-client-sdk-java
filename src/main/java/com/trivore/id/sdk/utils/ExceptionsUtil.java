package com.trivore.id.sdk.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trivore.id.sdk.exceptions.AuthenticationException;
import com.trivore.id.sdk.exceptions.ConflictException;
import com.trivore.id.sdk.exceptions.ForbiddenException;
import com.trivore.id.sdk.exceptions.MobileQuotaReachedException;
import com.trivore.id.sdk.exceptions.NotFoundException;
import com.trivore.id.sdk.exceptions.TrivoreIDErrorException;
import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.ErrorResponse;

import okhttp3.ResponseBody;

/**
 * Exceptions helper.
 */
public class ExceptionsUtil {

	/** ObjectMapper with sensible configuration */
	private static final ObjectMapper MAPPER = new ObjectMapper();

	private ExceptionsUtil() {
		// ...
	}

	/**
	 * Helper that generates TrivoreID exception based on the ClientErrorException.
	 *
	 * @param code    error status code
	 * @param message exception message
	 * @return TrivoreIDException
	 */
	public static TrivoreIDException generateTrivoreIDException(int code, String message) {

		try {
			message += " " + code + " " + message;

			if (code == 400) {
				return new TrivoreIDErrorException(message, code);
			} else if (code == 401) {
				return new AuthenticationException(message, code);
			} else if (code == 403) {
				return new ForbiddenException(message, code);
			} else if (code == 404) {
				return new NotFoundException(message, code);
			} else if (code == 409) {
				return new ConflictException(message, code);
			} else if (code == 429) {
				return new MobileQuotaReachedException(message, code);
			}
			return new TrivoreIDException(message, code);
		} catch (IllegalStateException e) {
			return new TrivoreIDException(message, code);
		}
	}

	/**
	 * Helper that generates TrivoreID exception based on the ClientErrorException.
	 *
	 * @param responseBody response body
	 * @return TrivoreIDException
	 */
	public static TrivoreIDException generateTrivoreIDException(ResponseBody responseBody) {
		if (responseBody == null) {
			return new TrivoreIDException("Empry response body.");
		}
		try {
			ErrorResponse response = MAPPER.readValue(responseBody.byteStream(), ErrorResponse.class);
			return new TrivoreIDException(response);
		} catch (Exception e) {
			return new TrivoreIDException("parseErrorResponse: Failed to parse error response");
		}
	}

}
