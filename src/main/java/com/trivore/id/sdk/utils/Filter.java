package com.trivore.id.sdk.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * A filter structure for filtering the results from the remote web service.
 * <p>
 * This filter class can be used to create filter structures that can be
 * executed on a remote web service. All filters are created with factory
 * functions that require mandatory components to build the certain filter.
 * </p>
 * <p>
 * These filters can be used together with {@link Criteria} objects, which also
 * provide a support to perform a pagination request to the remote service.
 * </p>
 * <p>
 * An example of using the filters.
 * </p>
 *
 * <pre>
 * Filter filter = Filter.and(Filter.equal("foo", "bar"), Filter.present("bar"));
 * </pre>
 */
public class Filter {

	/** The prefix to be added when specifying the filter as a URI attribute. */
	protected static final String FILTER_PREFIX = "filter=";

	private final FilterType type;
	private final String attribute;
	private final Object value;

	/**
	 * Construct a new filter with the given type, attribute and operator value.
	 *
	 * @param type      A non-null filter type definition.
	 * @param attribute A target attribute.
	 * @param value     The operator value.
	 * @throws IllegalArgumentException On invalid or unsupported arguments.
	 */
	public Filter(FilterType type, String attribute, Object value) throws IllegalArgumentException {
		Assert.notNull(type, "The type cannot be null");
		this.type = type;
		this.attribute = attribute;
		this.value = value;
	}

	/**
	 * Filter where target attribute value must match with the value.
	 *
	 * @param attribute The name of the target attribute.
	 * @param value     The target value.
	 * @return An equality comparison filter.
	 * @throws IllegalArgumentException On empty or null attribute name.
	 */
	public static Filter equal(String attribute, Object value) throws IllegalArgumentException {
		Assert.notNull(attribute, "Equal filter must have non-null attribute");
		Assert.hasText(attribute, "Equal filter must have non-empty attribute");
		return new Filter(FilterType.EQUAL, attribute, value);
	}

	/**
	 * Filter where target attribute value must not match with the value.
	 *
	 * @param attribute The name of the target attribute.
	 * @param value     The target value.
	 * @return A not-equal comparison filter.
	 * @throws IllegalArgumentException On empty or null attribute name.
	 */
	public static Filter notEqual(String attribute, Object value) throws IllegalArgumentException {
		Assert.notNull(attribute, "Not-equal filter must have non-null attribute");
		Assert.hasText(attribute, "Not-equal filter must have non-empty attribute");
		return new Filter(FilterType.NOT_EQUAL, attribute, value);
	}

	/**
	 * Filter where the value must be a substring of the target attribute value.
	 *
	 * @param attribute The name of the target attribute.
	 * @param value     The target value.
	 * @return A contains filter.
	 * @throws IllegalArgumentException On empty or null attribute or value.
	 */
	public static Filter contains(String attribute, String value) throws IllegalArgumentException {
		Assert.notNull(attribute, "Contains filter must have non-null attribute");
		Assert.hasText(attribute, "Contains filter must have non-empty attribute");
		Assert.notNull(value, "Contains filter must have non-null value");
		Assert.hasText(value, "Contains filter must have non-empty value");
		return new Filter(FilterType.CONTAINS, attribute, value);
	}

	/**
	 * Filter where value must be a substring at the start of the attribute value.
	 *
	 * @param attribute The name of the target attribute.
	 * @param value     The target value.
	 * @return A startsWith filter.
	 * @throws IllegalArgumentException On empty or null attribute or value.
	 */
	public static Filter startsWith(String attribute, String value) throws IllegalArgumentException {
		Assert.notNull(attribute, "StartsWith filter must have non-null attribute");
		Assert.hasText(attribute, "StartsWith filter must have non-empty attribute");
		Assert.notNull(value, "StartsWith filter must have non-null value");
		Assert.hasText(value, "StartsWith filter must have non-empty value");
		return new Filter(FilterType.STARTS_WITH, attribute, value);
	}

	/**
	 * Filter where value must be a substring at the end of the attribute value.
	 *
	 * @param attribute The name of the target attribute.
	 * @param value     The target value.
	 * @return A endsWith filter.
	 * @throws IllegalArgumentException On empty or null attribute or value.
	 */
	public static Filter endsWith(String attribute, String value) throws IllegalArgumentException {
		Assert.notNull(attribute, "EndsWith filter must have non-null attribute");
		Assert.hasText(attribute, "EndsWith filter must have non-empty attribute");
		Assert.notNull(value, "EndsWith filter must have non-null value");
		Assert.hasText(value, "EndsWith filter must have non-empty value");
		return new Filter(FilterType.ENDS_WITH, attribute, value);
	}

	/**
	 * Filter where the target attribute value must be non-empty and non-null.
	 *
	 * @param attribute The name of the target attribute.
	 * @return A present filter.
	 * @throws IllegalArgumentException On empty or null attribute.
	 */
	public static Filter present(String attribute) throws IllegalArgumentException {
		Assert.notNull(attribute, "Present With filter must have non-null attribute");
		Assert.hasText(attribute, "Present filter must have non-empty attribute");
		return new Filter(FilterType.PRESENT, attribute, null);
	}

	/**
	 * Filter where target attribute value must be less than the value.
	 *
	 * @param attribute The name of the target attribute.
	 * @param value     The target value.
	 * @return A less-than filter.
	 * @throws IllegalArgumentException On empty or null attribute or value.
	 */
	public static Filter lt(String attribute, Object value) throws IllegalArgumentException {
		Assert.notNull(attribute, "Less-than filter must have non-null attribute");
		Assert.hasText(attribute, "Less-than filter must have non-empty attribute");
		Assert.notNull(value, "Less-than filter must have non-null value");
		return new Filter(FilterType.LESS_THAN, attribute, value);
	}

	/**
	 * Filter where target attribute value must be less or equal than value.
	 *
	 * @param attribute The name of the target attribute.
	 * @param value     The target value.
	 * @return A less-or-equal-than filter.
	 * @throws IllegalArgumentException On empty or null attribute or value.
	 */
	public static Filter le(String attribute, Object value) throws IllegalArgumentException {
		Assert.notNull(attribute, "Less-or-equal-than filter must have non-null attribute");
		Assert.hasText(attribute, "Less-or-equal-than filter must have non-empty attribute");
		Assert.notNull(value, "Less-or-equal-than filter must have non-null value");
		return new Filter(FilterType.LESS_OR_EQUAL_THAN, attribute, value);
	}

	/**
	 * Filter where target attribute value must be greater than the value.
	 *
	 * @param attribute The name of the target attribute.
	 * @param value     The target value.
	 * @return A greater-than filter.
	 * @throws IllegalArgumentException On empty or null attribute or value.
	 */
	public static Filter gt(String attribute, Object value) throws IllegalArgumentException {
		Assert.notNull(attribute, "Greater-than filter must have non-null attribute");
		Assert.hasText(attribute, "Greater-than filter must have non-empty attribute");
		Assert.notNull(value, "Greater-than filter must have non-null value");
		return new Filter(FilterType.GREATER_THAN, attribute, value);
	}

	/**
	 * Filter where target attribute value must be greater or equal than value.
	 *
	 * @param attribute The name of the target attribute.
	 * @param value     The target value.
	 * @return A less-or-equal-than filter.
	 * @throws IllegalArgumentException On empty or null attribute or value.
	 */
	public static Filter ge(String attribute, Object value) throws IllegalArgumentException {
		Assert.notNull(attribute, "Greater-or-equal-than filter must have non-null attribute");
		Assert.hasText(attribute, "Greater-or-equal-than filter must have non-empty attribute");
		Assert.notNull(value, "Greater-or-equal-than filter must have non-null value");
		return new Filter(FilterType.GREATER_OR_EQUAL_THAN, attribute, value);
	}

	/**
	 * Filter which performs a logical and for the provided filters.
	 *
	 * @param filter1 The first filter.
	 * @param filter2 The second filter.
	 * @return An and filter.
	 * @throws IllegalArgumentException When either filter is null.
	 */
	public static Filter and(Filter filter1, Filter filter2) throws IllegalArgumentException {
		Assert.notNull(filter1, "And filter must contain a non-null first filter");
		Assert.notNull(filter2, "And filter must contain a non-null second filter");
		return new Filter(FilterType.AND, null, Arrays.asList(filter1, filter2));
	}

	/**
	 * Filter which performs a logical or for the provided filters.
	 *
	 * @param filter1 The first filter.
	 * @param filter2 The second filter.
	 * @return An or filter.
	 * @throws IllegalArgumentException When either filter is null.
	 */
	public static Filter or(Filter filter1, Filter filter2) throws IllegalArgumentException {
		Assert.notNull(filter1, "Or filter must contain a non-null first filter");
		Assert.notNull(filter2, "Or filter must contain a non-null second filter");
		return new Filter(FilterType.OR, null, Arrays.asList(filter1, filter2));
	}

	/**
	 * Get the {@link FilterType} of the filter.
	 * <p>
	 * Type specifies how the attribute and value definitions are handled.
	 * </p>
	 *
	 * @return The type of the filter.
	 */
	public FilterType getType() {
		return type;
	}

	/**
	 * Get the target attribute of the filter.
	 * <p>
	 * Target attribute defines the name of the remote target property.
	 * </p>
	 *
	 * @return The filter's attribute target.
	 */
	public String getAttribute() {
		return attribute;
	}

	/**
	 * Get the value of the filter.
	 * <p>
	 * The contents and the usage of the value depends on the filter type.
	 * </p>
	 *
	 * @return The value assigned for the filter.
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Build a filter string that can be passed to request query.
	 *
	 * @return A filter string to be passed to request query.
	 */
	public String toFilterString() {
		StringBuilder builder = new StringBuilder();
		if (value instanceof List<?>) {
			// iterate and build through the compound operator.
			@SuppressWarnings("unchecked")
			List<Filter> subFilters = (List<Filter>) value;
			int subFilterCount = subFilters.size();
			if (subFilterCount != 2) {
				throw new AssertionError("Compound operator has " + subFilterCount + " filters");
			}
			builder.append("(");
			builder.append(subFilters.get(0).toFilterString());
			builder.append(") ");
			builder.append(type.getOperator());
			builder.append(" (");
			builder.append(subFilters.get(1).toFilterString());
			builder.append(")");
		} else {
			// build a new "attribute operator[ value]" string.
			builder.append(attribute);
			builder.append(" ");
			builder.append(type.getOperator());
			if (value != null) {
				builder.append(" ");
				if (value instanceof Filter) {
					builder.append(((Filter) value).toFilterString());
				} else {
					builder.append("\"");
					builder.append(value);
					builder.append("\"");
				}
			}
		}
		return builder.toString();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		} else if (!(other instanceof Filter)) {
			return false;
		}
		Filter o = (Filter) other;
		return Objects.equals(type, o.type) && Objects.equals(attribute, o.attribute) && Objects.equals(value, o.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, attribute, value);
	}

	@Override
	public String toString() {
		return "Filter [type=" + type + ", attribute=" + attribute + ", value=" + value + "]";
	}

}
