package com.trivore.id.sdk.exceptions;

/**
 * A Trivore ID authentication exception.
 * <p>
 * This exception is typically thrown on errors that occur when the user/client
 * is not authenticated.
 * </p>
 */
@SuppressWarnings("serial")
public class ConflictException extends TrivoreIDException {

	/**
	 * Construct a new exception with the given messages, error and status code.
	 *
	 * @param message    The message to include.
	 * @param statusCode HTTP response code
	 */
	public ConflictException(String message, int statusCode) {
		super(message, statusCode);
	}
}
