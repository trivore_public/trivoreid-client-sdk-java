package com.trivore.id.sdk.exceptions;

/**
 * A Trivore ID Not Found exception.
 * <p>
 * These exceptions are typically thrown on errors that occur when the target
 * object was not found.
 * </p>
 */
@SuppressWarnings("serial")
public class NotFoundException extends TrivoreIDException {

	/**
	 * Construct a new exception with the given messages, error and status code.
	 *
	 * @param message    The message to include.
	 * @param statusCode HTTP response code
	 */
	public NotFoundException(String message, int statusCode) {
		super(message, statusCode);
	}
}