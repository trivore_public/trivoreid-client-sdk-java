package com.trivore.id.sdk.exceptions;

import com.trivore.id.sdk.models.ErrorResponse;

/**
 * A Trivore ID specific exception.
 * <p>
 * These exceptions are typically thrown on errors that occur on the service
 * layer that handles Trivore ID REST API specific tasks and communication.
 * </p>
 */
@SuppressWarnings("serial")
public class TrivoreIDException extends Exception {

	private int statusCode;
	private String errorMessage;
	private String errorCode;

	/**
	 * Construct a new exception without message or cause.
	 */
	public TrivoreIDException() {
		// ...
	}

	/**
	 * Construct a new exception with the given message.
	 *
	 * @param message The message to include.
	 */
	public TrivoreIDException(String message) {
		super(message);
	}

	/**
	 * Construct a new exception with the given error response.
	 *
	 * @param response error response
	 */
	public TrivoreIDException(ErrorResponse response) {
		super(response.getErrorMessage());
		this.statusCode = response.getStatusCode();
		this.errorCode = response.getErrorCode();
	}

	/**
	 * Construct a new exception with the given message and status code.
	 *
	 * @param message    The message to include.
	 * @param statusCode status code
	 */
	public TrivoreIDException(String message, int statusCode) {
		super(message);
		this.statusCode = statusCode;
	}

	/**
	 * Construct a new exception with the given messages, error and status code.
	 *
	 * @param message      The message to include.
	 * @param statusCode   HTTP response code
	 * @param errorMessage Human readable error message
	 * @param errorCode    Computer readable error code
	 */
	public TrivoreIDException(String message, int statusCode, String errorMessage, String errorCode) {
		super(message);
		this.statusCode = statusCode;
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}

	/**
	 * Construct a new exception with the given cause.
	 *
	 * @param cause The source cause of the exception.
	 */
	public TrivoreIDException(Throwable cause) {
		super(cause);
	}

	/**
	 * Get status code.
	 *
	 * @return status code
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * Get error message.
	 *
	 * @return status code
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Get error code.
	 *
	 * @return status code
	 */
	public String getErrorCode() {
		return errorCode;
	}

}
