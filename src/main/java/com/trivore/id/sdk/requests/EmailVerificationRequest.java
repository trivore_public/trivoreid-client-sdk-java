package com.trivore.id.sdk.requests;

import java.io.Serializable;
import java.util.Objects;

/**
 * A data transfer object (DTO) used to execute user email verification.
 * <p>
 * Used to execute the user email verification procedure at Trivore ID service.
 * </p>
 */
@SuppressWarnings("serial")
public final class EmailVerificationRequest implements Serializable {

	private String returnUrl;
	private Integer expirationDays;
	private String value;

	/**
	 * Construct a new email verification request object.
	 */
	public EmailVerificationRequest() {
		// ...
	}

	/**
	 * Construct a new email verification request object.
	 *
	 * @param returnUrl      The return URL for the email verification.
	 * @param expirationDays The amount of days before the email expires.
	 * @param value          The email address that shoudld be verified. The address
	 *                       must be one of the email addresses in the user emails
	 *                       list.
	 */
	public EmailVerificationRequest(String returnUrl, Integer expirationDays, String value) {
		this.returnUrl = returnUrl;
		this.expirationDays = expirationDays;
		this.value = value;
	}

	/**
	 * Get the return URL for the email verification.
	 * <p>
	 * URL where the user is directed after email is verified. If null, user is
	 * being redirected to the identification service front page.
	 * </p>
	 *
	 * @return The return URL for the email verification.
	 */
	public String getReturnUrl() {
		return returnUrl;
	}

	/**
	 * Set the return URL for the email verification.
	 * <p>
	 * URL where the user is directed after email is verified. If null, user is
	 * being redirected to the identification service front page.
	 * </p>
	 *
	 * @param returnUrl The return URL for the email verification.
	 */
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	/**
	 * Get the number of days the verification will be valid.
	 * <p>
	 * How many days until the verification link in the verification email expires.
	 * If not specified, current policies or default value is used.
	 * </p>
	 *
	 * @return The amount of days before the verification email expires.
	 */
	public Integer getExpirationDays() {
		return expirationDays;
	}

	/**
	 * Set the number of days the verification will be valid.
	 * <p>
	 * How many days until the verification link in the verification email expires.
	 * If not specified, current policies or default value is used.
	 * </p>
	 *
	 * @param expirationDays The amount of days before the email expires.
	 */
	public void setExpirationDays(Integer expirationDays) {
		this.expirationDays = expirationDays;
	}

	/**
	 * @return the email address that shoudld be verified. The address must be one
	 *         of the email addresses in the user emails list.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the email address that shoudld be verified. The address must be
	 *              one of the email addresses in the user emails list.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof EmailVerificationRequest)) {
			return false;
		}
		EmailVerificationRequest o = (EmailVerificationRequest) object;
		return Objects.equals(returnUrl, o.returnUrl) && Objects.equals(expirationDays, o.expirationDays);
	}

	@Override
	public int hashCode() {
		return Objects.hash(returnUrl, expirationDays);
	}

	@Override
	public String toString() {
		return "EmailVerificationRequest [returnUrl=" + returnUrl + ", expirationDays=" + expirationDays + "]";
	}

}
