package com.trivore.id.sdk.requests;

import java.io.Serializable;
import java.util.Objects;

/**
 * A data transfer object (DTO) used to execute user phone number verification.
 * <p>
 * Used to execute the user phone number verification procedure at Trivore ID
 * service.
 * </p>
 */
@SuppressWarnings("serial")
public class PhoneNumberVerificationRequest implements Serializable {

	private String returnUrl;
	private Integer expirationDays;
	private String value;

	/**
	 * Construct a new phone number verification request object.
	 */
	public PhoneNumberVerificationRequest() {
		// ...
	}

	/**
	 * Construct a new phone number verification request object.
	 *
	 * @param returnUrl      The return URL for the phone number verification.
	 * @param expirationDays The amount of days before the verification phone number
	 *                       expires.
	 * @param value          The phone number that shoudld be verified. The number
	 *                       must be one of the phone numberes in the user mobiles
	 *                       list
	 */
	public PhoneNumberVerificationRequest(String returnUrl, Integer expirationDays, String value) {
		this.returnUrl = returnUrl;
		this.expirationDays = expirationDays;
		this.value = value;
	}

	/**
	 * Get the return URL for the phone number verification.
	 * <p>
	 * URL where the user is directed after phone number is verified. If null, user
	 * is being redirected to the identification service front page.
	 * </p>
	 *
	 * @return The return URL for the phone number verification.
	 */
	public String getReturnUrl() {
		return returnUrl;
	}

	/**
	 * Set the return URL for the phone number verification.
	 * <p>
	 * URL where the user is directed after phone number is verified. If null, user
	 * is being redirected to the identification service front page.
	 * </p>
	 *
	 * @param returnUrl The return URL for the phone number verification.
	 */
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	/**
	 * Get the number of days the verification will be valid.
	 * <p>
	 * How many days until the verification link in the verification phone number
	 * expires. If not specified, current policies or default value is used.
	 * </p>
	 *
	 * @return The amount of days before the verification phone number expires.
	 */
	public Integer getExpirationDays() {
		return expirationDays;
	}

	/**
	 * Set the number of days the verification will be valid.
	 * <p>
	 * How many days until the verification link in the verification phone number
	 * expires. If not specified, current policies or default value is used.
	 * </p>
	 *
	 * @param expirationDays The amount of days before the phone number expires.
	 */
	public void setExpirationDays(Integer expirationDays) {
		this.expirationDays = expirationDays;
	}

	/**
	 * @return The phone number that shoudld be verified. The number must be one of
	 *         the phone numberes in the user mobiles list
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value The phone number that shoudld be verified. The number must be
	 *              one of the phone numberes in the user mobiles list
	 */
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof PhoneNumberVerificationRequest)) {
			return false;
		}
		PhoneNumberVerificationRequest o = (PhoneNumberVerificationRequest) object;
		return Objects.equals(returnUrl, o.returnUrl) && Objects.equals(expirationDays, o.expirationDays);
	}

	@Override
	public int hashCode() {
		return Objects.hash(returnUrl, expirationDays);
	}

	@Override
	public String toString() {
		return "PhoneNumberVerificationRequest [returnUrl=" + returnUrl + ", expirationDays=" + expirationDays + "]";
	}

}
