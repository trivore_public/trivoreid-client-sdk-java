package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.EmailTemplate;
import com.trivore.id.sdk.models.EmailTemplateSendOptions;
import com.trivore.id.sdk.models.Page;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface EmailTemplateService {

	@GET("emailtemplate")
	Call<Page<EmailTemplate>> getAll(@Query("startIndex") Integer startIndex,
										   @Query("count") Integer count,
										   @Query("filter") String filter);
	
	@GET("emailtemplate/{emailTemplateId}")
	Call<EmailTemplate> get(@Path("emailTemplateId") String id);
	
	@POST("emailtemplate/{emailTemplateId}/send")
	Call<Void> send(@Path("emailTemplateId") String templateId, @Body EmailTemplateSendOptions sendOptions);
}
