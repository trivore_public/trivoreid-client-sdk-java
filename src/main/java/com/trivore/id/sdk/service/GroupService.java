package com.trivore.id.sdk.service;

import java.util.List;

import com.trivore.id.sdk.models.Group;
import com.trivore.id.sdk.models.Page;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * A group service to process different kinds of group specific operations.
 * <p>
 * Handles group specific tasks and communicates with the Trivore ID service.
 * </p>
 */
public interface GroupService {

	/**
	 * Get all groups from accessible namespaces.
	 *
	 * @return page with group objects
	 */
	@GET("group")
	Call<Page<Group>> getAll();

	/**
	 * Get all groups satisfying the provided filter.
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query groups.
	 * @return page with group objects
	 */
	@GET("group")
	Call<Page<Group>> getAll(
			@Query("startIndex") Integer startIndex,
			@Query("count") Integer count,
			@Query("filter") String filter);

	/**
	 * Add a new group into the service.
	 * <p>
	 * If the namespace code is defined, then the list of groups (memberOf) will be
	 * checked, and if group id or name within namespase doesn't exist, then new
	 * group will be created.
	 * </p>
	 *
	 * @param group The group to be added.
	 * @return A group saved in Trivore ID.
	 */
	@POST("group")
	Call<Group> create(@Body Group group);

	/**
	 * Get the group with the given identifier.
	 *
	 * @param groupId The id of the target group.
	 * @return The group with the given identifier.
	 */
	@GET("group/{groupId}")
	Call<Group> get(@Path("groupId") String groupId);

	/**
	 * Save the changes of the provided group.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided group.
	 * </p>
	 *
	 * @param groupId the ID of the target group
	 * @param group   The target group.
	 * @return A group saved in Trivore ID.
	 */
	@PUT("group/{groupId}")
	Call<Group> update(@Path("groupId") String groupId, @Body Group group);

	/**
	 * Delete a group from Trivore ID.
	 *
	 * @param groupId The id of the target group.
	 * @return void
	 */
	@DELETE("group/{groupId}")
	Call<Void> delete(@Path("groupId") String groupId);

	/**
	 * Get all subgroups of the target group.
	 *
	 * @param groupId The id of the target group.
	 * @return List with the subgroups of the target group.
	 */
	@GET("group/{groupId}/nested")
	Call<List<Group>> getNested(@Path("groupId") String groupId);

}
