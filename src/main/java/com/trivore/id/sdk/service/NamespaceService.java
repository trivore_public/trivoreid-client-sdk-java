package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.Page;
import com.trivore.id.sdk.models.namespace.Namespace;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * A namespace service for all kinds of namespace management operations.
 * <p>
 * Handles all kinds of operations with Trivore ID namespace objects.
 * </p>
 */
public interface NamespaceService {

	/**
	 * Get all accessible namespaces.
	 *
	 * @return page with namespace objects
	 */
	@GET("namespace")
	Call<Page<Namespace>> getAllAccessible();

	/**
	 * Get all namespaces satisfying the provided criteria.
	 *
	 * @param startIndex pagination start index
	 * @param count      pagination count
	 * @param filter     The filter query namespaces.
	 * @return page with namespace objects
	 */
	@GET("namespace")
	Call<Page<Namespace>> getAll( //
			@Query("startIndex") Integer startIndex, //
			@Query("count") Integer count, //
			@Query("filter") String filter);

	/**
	 * Add a new namespace into the service.
	 *
	 * @param namespace The namespace to be added.
	 * @return A namespace saved in Trivore ID.
	 */
	@POST("namespace")
	Call<Namespace> create(@Body Namespace namespace);

	/**
	 * Get the namespace with the given identifier.
	 *
	 * @param namespaceId The id of the target namespace.
	 * @return The namespace with the given identifier.
	 */
	@GET("namespace/{namespaceId}")
	Call<Namespace> get(@Path("namespaceId") String namespaceId);

	/**
	 * Save the changes of the provided namespace.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) the provided namespace.
	 * </p>
	 *
	 * @param namespaceId the ID of the target namespace
	 * @param namespace   The target namespace.
	 * @return A namespace saved in Trivore ID.
	 */
	@PUT("namespace/{namespaceId}")
	Call<Namespace> update(@Path("namespaceId") String namespaceId, @Body Namespace namespace);

	/**
	 * Delete a namespace from Trivore ID.
	 *
	 * @param namespaceId The id of the target namespace.
	 * @return void
	 */
	@DELETE("namespace/{namespaceId}")
	Call<Void> delete(@Path("namespaceId") String namespaceId);
}
