package com.trivore.id.sdk.service;

import com.trivore.id.sdk.models.user.profile.Profile;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * A profile service to process different kinds of profile specific operations.
 * <p>
 * Handles profile specific tasks and communicates with the Trivore ID service.
 * </p>
 */
public interface ProfileService {

	/**
	 * Get the user profile with the given identifier.
	 *
	 * @param userId The id of the target user.
	 * @return The user with the given identifier.
	 */
	@GET("user/{userId}/profile")
	Call<Profile> get(@Path("userId") String userId);

	/**
	 * Update user profile.
	 * <p>
	 * Requests Trivore ID to save (i.e. update) user profile.
	 * </p>
	 *
	 * @param userId  The id of the target user.
	 * @param profile The user profile.
	 * @return A user profile saved in Trivore ID.
	 */
	@PUT("user/{userId}/profile")
	Call<Profile> update(@Path("userId") String userId, @Body Profile profile);

}
