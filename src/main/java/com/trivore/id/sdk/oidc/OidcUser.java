package com.trivore.id.sdk.oidc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trivore.id.sdk.models.user.Address;
import com.trivore.id.sdk.models.user.Consents;
import com.trivore.id.sdk.models.user.Name;

/**
 * An object which represents OIDC User.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class OidcUser implements Serializable {

	private static final String NAMESPACE_CLAIM = "https://oneportal.trivore.com/claims/namespace";
	private static final String GROUPS_CLAIM = "https://oneportal.trivore.com/claims/groups";
	private static final String CONCENTS_CLAIM = "https://oneportal.trivore.com/claims/consents";
	private static final String MINOR_CLAIM = "https://oneportal.trivore.com/claims/minor";

	private String id;
	private String username;
	private String email;
	private boolean emailVerified;
	private String phone;
	private boolean phoneNumberVerified;
	private Name names;
	private String nsCode;
	private String birthdate;
	private Address address;
	private Consents consents;
	private List<String> groupNames;
	private Boolean minor;
	private String locale;
	private String nickname;
	private String zoneinfo;

	/**
	 * Construct OIDC User.
	 */
	public OidcUser() {

	}

	/**
	 * Construct OIDC User.
	 *
	 * @param map map with the OIDC user data
	 */
	@SuppressWarnings("unchecked")
	public OidcUser(Map<String, Object> map) {
		ObjectMapper mapper = new ObjectMapper();
		for (Entry<String, Object> entry : map.entrySet()) {
			if (entry.getKey().contains(NAMESPACE_CLAIM)) {
				this.nsCode = (String) entry.getValue();
			} else if (entry.getKey().contains("sub")) {
				this.id = (String) entry.getValue();
			} else if (entry.getKey().contains("family_name")) {
				getNames().setFamilyName((String) entry.getValue());
			} else if (entry.getKey().contains("given_name")) {
				getNames().setGivenName((String) entry.getValue());
			} else if (entry.getKey().contains("middle_name")) {
				getNames().setMiddleName((String) entry.getValue());
			} else if (entry.getKey().contains("preferred_username")) {
				this.username = (String) entry.getValue();
			} else if (entry.getKey().contains("birthdate")) {
				this.birthdate = (String) entry.getValue();
			} else if (entry.getKey().contains("phone_number") && !entry.getKey().contains("verified")) {
				this.phone = (String) entry.getValue();
			} else if (entry.getKey().contains("email") && !entry.getKey().contains("verified")) {
				this.email = (String) entry.getValue();
			} else if (entry.getKey().contains("phone_number_verified")) {
				this.emailVerified = (Boolean) entry.getValue();
			} else if (entry.getKey().contains("email_verified")) {
				this.phoneNumberVerified = (Boolean) entry.getValue();
			} else if (entry.getKey().contains(GROUPS_CLAIM)) {
				this.groupNames = (ArrayList<String>) entry.getValue();
			} else if (entry.getKey().contains(CONCENTS_CLAIM)) {
				this.consents = mapper.convertValue(entry.getValue(), Consents.class);
			} else if (entry.getKey().contains(MINOR_CLAIM)) {
				this.minor = (Boolean) entry.getValue();
			} else if (entry.getKey().contains("locale")) {
				this.locale = (String) entry.getValue();
			} else if (entry.getKey().contains("nickname")) {
				this.nickname = (String) entry.getValue();
			} else if (entry.getKey().contains("zoneinfo")) {
				this.zoneinfo = (String) entry.getValue();
			} else if (entry.getKey().contains("address")) {
				address = new Address();
				@SuppressWarnings("unchecked")
				Map<String, String> addr = (Map<String, String>) entry.getValue();
				if (addr.containsKey("country")) {
					address.setCountry((String) addr.get("country"));
				}
				if (addr.containsKey("locality")) {
					address.setLocality((String) addr.get("locality"));
				}
				if (addr.containsKey("postal_code")) {
					address.setPostalCode((String) addr.get("postal_code"));
				}
				if (addr.containsKey("region")) {
					address.setRegion((String) addr.get("region"));
				}
				if (addr.containsKey("street_address")) {
					address.setStreetAddress(addr.get("street_address"));
				}
			}
		}
	}

	/**
	 * Get OUDC user ID.
	 *
	 * @return OUDC user ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set OUDC user ID.
	 *
	 * @param id OUDC user ID
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get OUDC user names.
	 *
	 * @return OUDC user names.
	 */
	public Name getNames() {
		if (names == null) {
			names = new Name();
		}
		return names;
	}

	/**
	 * Set OUDC user names.
	 *
	 * @param names OUDC user names.
	 */
	public void setNames(Name names) {
		this.names = names;
	}

	/**
	 * Get OUDC user email.
	 *
	 * @return OUDC user email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Set OUDC user email.
	 *
	 * @param email OUDC user email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Get if OUDC user email is verified.
	 *
	 * @return if OUDC email is verified
	 */
	public boolean isEmailVerified() {
		return emailVerified;
	}

	/**
	 * Get OUDC user phone number.
	 * 
	 * @return OUDC user phone number
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Set OUDC user phone number.
	 * 
	 * @param phone OUDC user phone number
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Get if OUDC user phone is verified.
	 * 
	 * @return if OUDC user phone is verified
	 */
	public boolean isPhoneNumberVerified() {
		return phoneNumberVerified;
	}

	/**
	 * Get OUDC user username.
	 * 
	 * @return OUDC user username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Set OUDC user username.
	 * 
	 * @param username OUDC user username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Get OUDC user nsCode.
	 * 
	 * @return OUDC user nsCode
	 */
	public String getNsCode() {
		return nsCode;
	}

	/**
	 * Set OUDC user nsCode.
	 * 
	 * @param nsCode OUDC user nsCode
	 */
	public void setNsCode(String nsCode) {
		this.nsCode = nsCode;
	}

	/**
	 * Get OUDC user birthdate.
	 * 
	 * @return OUDC user birthdate
	 */
	public String getBirthdate() {
		return birthdate;
	}

	/**
	 * Set OUDC user birthdate.
	 * 
	 * @param birthdate OUDC user birthdate
	 */
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 * @return address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return consents
	 */
	public Consents getConsents() {
		return consents;
	}

	/**
	 * @param consents
	 */
	public void setConsents(Consents consents) {
		this.consents = consents;
	}

	/**
	 * @return group names
	 */
	public List<String> getGroupNames() {
		return groupNames;
	}

	/**
	 * @param groupNames
	 */
	public void setGroupNames(List<String> groupNames) {
		this.groupNames = groupNames;
	}

	/**
	 * @return minor status
	 */
	public Boolean isMinor() {
		return minor;
	}

	/**
	 * @param minor
	 */
	public void setMinor(Boolean minor) {
		this.minor = minor;
	}

	/**
	 * @return locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @return nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param nickname
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @return zoneinfo
	 */
	public String getZoneinfo() {
		return zoneinfo;
	}

	/**
	 * @param zoneinfo
	 */
	public void setZoneinfo(String zoneinfo) {
		this.zoneinfo = zoneinfo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof OidcUser)) {
			return false;
		}
		OidcUser o = (OidcUser) obj;
		return Objects.equals(id, o.id);
	}

	@Override
	public String toString() {
		return "OidcUser [id=" + id + ", username=" + username + ", email=" + email + ", emailVerified=" + emailVerified
				+ ", phone=" + phone + ", phoneNumberVerified=" + phoneNumberVerified + ", names=" + names + ", nsCode="
				+ nsCode + ", birthdate=" + birthdate + ", address=" + address + ", consents=" + consents
				+ ", groupNames=" + groupNames + ", minor=" + minor + ", locale=" + locale + ", nickname=" + nickname
				+ ", zoneinfo=" + zoneinfo + "]";
	}

}
