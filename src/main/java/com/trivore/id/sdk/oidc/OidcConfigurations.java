package com.trivore.id.sdk.oidc;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A response result encapsulation for the OpenID Connect (OIDC) discovery.
 * <p>
 * This result container contains the results from the discovery end point.
 * Results include the mandatory paths for the authorisation, JWK, token and
 * user information. Values can be queried with {@link OidcClient}.
 * </p>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OidcConfigurations {

	@JsonProperty("issuer")
	private String issuer;

	@JsonProperty("authorization_endpoint")
	private String authorizationUri;

	@JsonProperty("jwks_uri")
	private String jwkSetUri;

	@JsonProperty("token_endpoint")
	private String tokenUri;

	@JsonProperty("userinfo_endpoint")
	private String userInfoUri;

	@JsonProperty("scopes_supported")
	private List<String> scopes;

	/**
	 * Construct a new empty OIDC discovery result.
	 */
	public OidcConfigurations() {
		// ...
	}

	/**
	 * Get the issuer of the OIDC discovery.
	 *
	 * @return The issuer of the OIDC discovery.
	 */
	public String getIssuer() {
		return issuer;
	}

	/**
	 * Set the issuer of the OIDC discovery.
	 *
	 * @param issuer The issuer of the OIDC discovery.
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	/**
	 * Get the OIDC authorisation URI.
	 *
	 * @return The OIDC authorisation URI.
	 */
	public String getAuthorizationUri() {
		return authorizationUri;
	}

	/**
	 * Set the OIDC authorisation URI.
	 *
	 * @param authorizationUri The OIDC authorisation URI.
	 */
	public void setAuthorizationUri(String authorizationUri) {
		this.authorizationUri = authorizationUri;
	}

	/**
	 * Get the OIDC JWK URI.
	 *
	 * @return The OIDC JWK URI.
	 */
	public String getJwkSetUri() {
		return jwkSetUri;
	}

	/**
	 * Set the OIDC JWK URI.
	 *
	 * @param jwkSetUri The OIDC JWK URI.
	 */
	public void setJwkSetUri(String jwkSetUri) {
		this.jwkSetUri = jwkSetUri;
	}

	/**
	 * Get the OIDC token URI.
	 *
	 * @return The OIDC token URI.
	 */
	public String getTokenUri() {
		return tokenUri;
	}

	/**
	 * Set the OIDC token URI.
	 *
	 * @param tokenUri The OIDC token URI.
	 */
	public void setTokenUri(String tokenUri) {
		this.tokenUri = tokenUri;
	}

	/**
	 * Get the user info URI.
	 *
	 * @return The user info URI.
	 */
	public String getUserInfoUri() {
		return userInfoUri;
	}

	/**
	 * Set the user info URI.
	 *
	 * @param userInfoUri The user info URI.
	 */
	public void setUserInfoUri(String userInfoUri) {
		this.userInfoUri = userInfoUri;
	}

	/**
	 * Get list of all scopes.
	 *
	 * @return list of all scopes
	 */
	public List<String> getScopes() {
		if (scopes == null) {
			scopes = new ArrayList<>();
		}
		return scopes;
	}

	/**
	 * Set list of all scopes.
	 *
	 * @param scopes list of all scopes
	 */
	public void setScopes(List<String> scopes) {
		this.scopes = scopes;
	}

	/**
	 * Get string with all scopes.
	 *
	 * @return string with scopes.
	 */
	public String getScopeString() {
		return scopes.toString().replace("[", "").replace("]", "").replace(",", "");
	}

	@Override
	public int hashCode() {
		return Objects.hash(issuer, authorizationUri, jwkSetUri, tokenUri, userInfoUri, scopes);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (!(obj instanceof OidcConfigurations)) {
			return false;
		}
		OidcConfigurations o = (OidcConfigurations) obj;
		return Objects.equals(issuer, o.issuer) && Objects.equals(authorizationUri, o.authorizationUri)
				&& Objects.equals(jwkSetUri, o.jwkSetUri) && Objects.equals(tokenUri, o.tokenUri)
				&& Objects.equals(userInfoUri, o.userInfoUri) && Objects.equals(scopes, o.scopes);
	}

	@Override
	public String toString() {
		return "OidcConfigurations [issuer=" + issuer + ", authorizationUri=" + authorizationUri + ", jwkSetUri="
				+ jwkSetUri + ", tokenUri=" + tokenUri + ", userInfoUri=" + userInfoUri + ", scopes=" + scopes + "]";
	}

}
