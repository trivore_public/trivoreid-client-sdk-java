package com.trivore.id.sdk.oidc;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.utils.ExceptionsUtil;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Helper class for the OIDC authorization.
 */
public class OidcClient {
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private Retrofit retrofit = null;
	private TrivoreIDConfigurations config;

	private String accessToken;

	/**
	 * Construct OIDC helper.
	 *
	 * @param server server URI
	 */
	public OidcClient(String server) {
		retrofit = new Retrofit.Builder() //
				.baseUrl(server) //
				.addConverterFactory(JacksonConverterFactory.create()) //
				.build();

		config = retrofit.create(TrivoreIDConfigurations.class);
	}

	/**
	 * Get the OIDC client configurations.
	 *
	 * @return OIDC client configurations
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException in case of network or server error
	 */
	public OidcConfigurations getConfigurations() throws IOException, TrivoreIDException {

		Response<OidcConfigurations> response = config.configurations().execute();

		if (response.isSuccessful()) {
			logger.info("Successfully found OIDC configurations.");
			return response.body();
		} else {
			logger.error("Failed find OIDC configurations.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Authorize and get access token using Password Grant authorization.
	 * <p>
	 * By default Password Grant authorization is disabled for the Oidc clients. Ask
	 * administrator to enable it.
	 * </p>
	 *
	 * @param clientId     OIDC client unique identifier
	 * @param clientSecret OIDC client secret
	 * @param username     user's username
	 * @param password     user's password
	 * @param scope        scope string (multiple skopes should be separated by the
	 *                     space)
	 * @return access token
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException in case of network or server error
	 */
	public String authorize(String clientId, String clientSecret, String username, String password, String scope)
			throws IOException, TrivoreIDException {

		String authorization = Credentials.basic(clientId, clientSecret);
		Map<String, String> data = new HashMap<>();
		data.put("grant_type", "password");
		data.put("scope", scope);
		data.put("username", username);
		data.put("password", password);

		Response<Map<String, String>> response = config
				.accessToken("application/x-www-form-urlencoded", authorization, data).execute();

		if (response.isSuccessful()) {
			accessToken = response.body().get("access_token");
			logger.info("Successfully get access token.");
			return accessToken;
		} else {
			logger.error("Failed to get access token.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}
	}

	/**
	 * Get the information about the authorized user.
	 *
	 * @return OIDC user profile
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException in case of network or server error
	 */
	public OidcUser getOidcUser() throws IOException, TrivoreIDException {
		return getOidcUser(accessToken);
	}

	/**
	 * Get the information about the authorized user.
	 *
	 * @param accessToken access token
	 * @return OIDC user profile
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException in case of network or server error
	 */
	public OidcUser getOidcUser(String accessToken) throws IOException, TrivoreIDException {

		String authorization = "Bearer " + accessToken;

		Response<Map<String, Object>> response = config.oidcUser(authorization).execute();

		if (response.isSuccessful()) {
			logger.info("Successfully got OIDC user.");
			return new OidcUser(response.body());
		} else {
			logger.error("Failed to get OIDC user.");
			throw ExceptionsUtil.generateTrivoreIDException(response.errorBody());
		}

	}

	private interface TrivoreIDConfigurations {

		@GET(".well-known/openid-configuration")
		public Call<OidcConfigurations> configurations();

		@POST("openid/token")
		public Call<Map<String, String>> accessToken( //
				@Header("Content-Type") String contentType, //
				@Header("Authorization") String authorization, //
				@QueryMap Map<String, String> data);

		@GET("openid/userinfo")
		public Call<Map<String, Object>> oidcUser(@Header("Authorization") String authorization);
	}

}
