package com.trivore.id.sdk;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.Properties;

import javax.annotation.CheckForNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.trivore.id.sdk.exceptions.TrivoreIDException;
import com.trivore.id.sdk.models.ErrorResponse;
import com.trivore.id.sdk.oidc.OidcClient;
import com.trivore.id.sdk.oidc.OidcConfigurations;
import com.trivore.id.sdk.oidc.OidcUser;
import com.trivore.id.sdk.service.DataStorageService;
import com.trivore.id.sdk.service.EmailService;
import com.trivore.id.sdk.service.EmailTemplateService;
import com.trivore.id.sdk.service.GroupService;
import com.trivore.id.sdk.service.MyDataService;
import com.trivore.id.sdk.service.NamespaceService;
import com.trivore.id.sdk.service.ProfileService;
import com.trivore.id.sdk.service.SMSService;
import com.trivore.id.sdk.service.UserService;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okhttp3.Route;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Trivore ID client for executing REST API requests.
 */
public class TrivoreID {
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	/** ObjectMapper with sensible configuration */
	private static final ObjectMapper MAPPER = new ObjectMapper();

	private String clientId;

	static {
		// Don't fail if receive unknown properties
		MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		// To keep same behaviour as with moxy, now Dates are output as
		// "2019-02-18T10:55:36.254+0000"
		MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		// Don't output nulls
		MAPPER.setSerializationInclusion(Include.NON_NULL);

		// Additional conversion modules:

		// Support Optional classes
		MAPPER.registerModule(new Jdk8Module());
		// Support Java 8 JSR-310 date/time classes
		MAPPER.registerModule(new JavaTimeModule());
		// Understand JAX-RS annotations (like @XmlElement)
		MAPPER.registerModule(new JaxbAnnotationModule());
	}

	private final Retrofit retrofit;
	private static OidcUser oidcUser;

	private TrivoreID(Retrofit retrofit, String clientId) {
		// Hidden constructor
		this.retrofit = retrofit;
		this.clientId = clientId;
	}

	/**
	 * Parse error response.
	 *
	 * @param responseBody response body
	 * @return error response
	 */
	public static ErrorResponse parseErrorResponse(ResponseBody responseBody) {
		if (responseBody == null) {
			return null;
		}
		try {
			return MAPPER.readValue(responseBody.byteStream(), ErrorResponse.class);
		} catch (Exception e) {
			logger.warn("parseErrorResponse: Failed to parse error response", e);
			return null;
		}
	}

	/**
	 * Get user service.
	 *
	 * @return user service
	 */
	public UserService userService() {
		return retrofit.create(UserService.class);
	}

	/**
	 * Get namespace service.
	 *
	 * @return namespace service
	 */
	public NamespaceService namespaceService() {
		return retrofit.create(NamespaceService.class);
	}

	/**
	 * Get group service.
	 *
	 * @return group service
	 */
	public GroupService groupService() {
		return retrofit.create(GroupService.class);
	}

	/**
	 * Get datastorage service.
	 *
	 * @return datastorage service
	 */
	public DataStorageService dataStorageService() {
		return retrofit.create(DataStorageService.class);
	}

	/**
	 * Get email service.
	 *
	 * @return email service
	 */
	public EmailService emailService() {
		return retrofit.create(EmailService.class);
	}
	
	public EmailTemplateService emailTemplateService() {
		return retrofit.create(EmailTemplateService.class);
	}

	/**
	 * Get mydata service.
	 *
	 * @return mydata service
	 */
	public MyDataService myDataService() {
		return retrofit.create(MyDataService.class);
	}

	/**
	 * Get profile service.
	 *
	 * @return profile service
	 */
	public ProfileService profileService() {
		return retrofit.create(ProfileService.class);
	}

	/**
	 * Get sms service.
	 *
	 * @return sms service
	 */
	public SMSService smsService() {
		return retrofit.create(SMSService.class);
	}

	/**
	 * Get the authenticated user's information.
	 *
	 * @return authenticated user's information
	 */
	public OidcUser getOidcUser() {
		return oidcUser;
	}

	/**
	 * Create client with Management API credentials from the default properties
	 * path (/etc/trivoreid/client_sdk.properties).
	 *
	 * @return SDK for management API client credentials
	 * @throws IOException if a problem occurred talking to the server.
	 */
	public static TrivoreID mgmtApiClient() throws IOException {
		return mgmtApiClient("/etc/trivoreid/client_sdk.properties");
	}

	/**
	 * Create client with Management API credentials from the default properties.
	 *
	 * @param path path to the configuration file
	 * @return SDK for management API client credentials
	 * @throws IOException if a problem occurred talking to the server.
	 */
	public static TrivoreID mgmtApiClient(String path) throws IOException {
		logger.debug("mgmtApiClient: Creating SDK for management api client credentials");

		String server = null;
		String clientId = null;
		String clientSecret = null;

		InputStream input = new FileInputStream(path);

		Properties properties = new Properties();
		properties.load(input);

		server = properties.getProperty("service.address");
		clientId = properties.getProperty("mgmtapi.id");
		clientSecret = properties.getProperty("mgmtapi.secret");

		return mgmtApiClient(server, clientId, clientSecret);
	}

	/**
	 * Create client with Management API credentials from the default properties.
	 *
	 * @param server       server url
	 * @param clientId     client id
	 * @param clientSecret client secret
	 * @return SDK for management API client credentials
	 */
	public static TrivoreID mgmtApiClient(String server, String clientId, String clientSecret) {
		OkHttpClient mgmtApiAuthClient = new OkHttpClient.Builder()
				.authenticator(new BasicAuthenticator(clientId, clientSecret)).build();

		String baseUrl = server + "/api/rest/v1/";

		Retrofit retrofit = new Retrofit.Builder()
				// Set base url
				.baseUrl(baseUrl)
				// Use above configured OkHttp client
				.client(mgmtApiAuthClient)
				// Use Jackson for json serialisation
				.addConverterFactory(JacksonConverterFactory.create(MAPPER)).build();

		return new TrivoreID(retrofit, clientId);
	}

	/**
	 * Create Trivore ID client wiht the password grant credentials using the
	 * default properties path.
	 * <p>
	 * The default properties path (/etc/trivoreid/client_sdk.properties).
	 * <p>
	 * The configurations:
	 * <ul>
	 * <li><b>service.address</b> server url
	 * <li><b>oidc.client.id</b> client id
	 * <li><b>oidc.client.secret</b> client secret
	 * <li><b>oidc.client.scope</b> scope
	 * <li><b>password.grant.username</b> username
	 * <li><b>password.grant.password</b> password
	 * </ul>
	 *
	 * @return SDK for OIDC access token
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public static TrivoreID openIdClient() throws IOException, TrivoreIDException {
		return openIdClient("/etc/trivoreid/client_sdk.properties");
	}

	/**
	 * Create Trivore ID client wiht the password grant credentials.
	 * <p>
	 * The configurations:
	 * <ul>
	 * <li><b>service.address</b> server url
	 * <li><b>oidc.client.id</b> client id
	 * <li><b>oidc.client.secret</b> client secret
	 * <li><b>oidc.client.scope</b> scope
	 * <li><b>password.grant.username</b> username
	 * <li><b>password.grant.password</b> password
	 * </ul>
	 *
	 * @param path path to the configuration file
	 * @return SDK for OIDC access token
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 * @throws IOException        if a problem occurred talking to the server.
	 */
	public static TrivoreID openIdClient(String path) throws IOException, TrivoreIDException {
		logger.debug("openIdClient: Creating SDK for OIDC client credentials");

		InputStream input = new FileInputStream(path);

		Properties properties = new Properties();
		properties.load(input);

		String server = properties.getProperty("service.address");
		String clientId = properties.getProperty("oidc.client.id");
		String clientSecret = properties.getProperty("oidc.client.secret");
		String scope = properties.getProperty("oidc.client.scope");
		String username = properties.getProperty("password.grant.username");
		String password = properties.getProperty("password.grant.password");

		return openIdClient(server, clientId, clientSecret, scope, username, password);
	}

	/**
	 * Create Trivore ID client wiht the password grant credentials.
	 *
	 * @param server       server url
	 * @param clientId     client id
	 * @param clientSecret client secret
	 * @param scope        scope
	 * @param username     username
	 * @param password     password
	 * @return SDK for OIDC access token
	 * @throws IOException        if a problem occurred talking to the server.
	 * @throws TrivoreIDException In a case of network or TrivoreID error.
	 */
	public static TrivoreID openIdClient(String server, String clientId, String clientSecret, String scope,
			String username, String password) throws IOException, TrivoreIDException {
		OidcClient oidc = new OidcClient(server);

		if (scope == null) {
			OidcConfigurations config = oidc.getConfigurations();
			scope = config.getScopeString();
		}

		String accessToken = oidc.authorize(clientId, clientSecret, username, password, scope);
		oidcUser = oidc.getOidcUser();

		return openIdClient(server, accessToken, clientId);
	}

	/**
	 * Create Trivore ID client wiht the password grant credentials.
	 *
	 * @param server      uri to the base server
	 * @param accessToken access token
	 * @return SDK for OIDC access token
	 */
	public static TrivoreID openIdClient(String server, String accessToken) {
		return openIdClient(server, accessToken, null);
	}

	private static TrivoreID openIdClient(String server, String accessToken, String clientId) {
		OkHttpClient oidcApiAuthClient = new OkHttpClient.Builder().authenticator(new BearerAuthenticator(accessToken))
				.build();

		String baseUrl = server + "/api/rest/v1/";

		Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).client(oidcApiAuthClient)
				.addConverterFactory(JacksonConverterFactory.create(MAPPER)).build();

		return new TrivoreID(retrofit, null);
	}

	private static class BasicAuthenticator implements Authenticator {

		private final String username;
		private final String password;

		public BasicAuthenticator(String username, String password) {
			this.username = username;
			this.password = password;
		}

		@Override
		public Request authenticate(@CheckForNull Route route, okhttp3.Response response) throws IOException {
			if (response.request().header("Authorization") != null) {
				return null; // Give up, we've already attempted to authenticate.
			}

			// This adds Basic auth header to request
			String credential = Credentials.basic(username, password);
			return response.request().newBuilder().header("Authorization", credential).build();
		}

	}

	private static class BearerAuthenticator implements Authenticator {

		private final String accessToken;

		/**
		 * 
		 * @param accessToken access token
		 */
		public BearerAuthenticator(String accessToken) {
			this.accessToken = accessToken;
		}

		@Override
		public Request authenticate(@CheckForNull Route route, okhttp3.Response response) throws IOException {
			if (response.request().header("Authorization") != null) {
				return null; // Give up, we've already attempted to authenticate.
			}

			return response.request().newBuilder().header("Authorization", "Bearer " + accessToken).build();
		}

	}

	/**
	 * Management API or OIDC client ID.
	 *
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * Management API or OIDC client ID.
	 *
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
}
