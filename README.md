## Trivore ID Client SDK

The Client SDK that wraps around the TrivoreID APIs.

This Client SDK covers only the most basic API requests. The rest endpoints are
covered in the [extended version](https://gitlab.com/trivore_public/trivoreid-extension-client-sdk-java).

Documentation to get started and use Services can be found [here](https://trivore.atlassian.net/wiki/spaces/TISpubdoc/pages/20515293/Client+SDK+for+Java).

### Maven Dependency

History of all releases can be found [here](https://mvnrepository.com/artifact/com.trivore/trivoreid-sdk).

```
<dependency>
    <groupId>com.trivore</groupId>
    <artifactId>trivoreid-sdk</artifactId>
    <version>2.2.1</version>
</dependency>
```

### Development

#### Publishing a new version

To publish a new version of the project run `mvn release:prepare -P release` and after that completes successfully, run `mvn release:perform -P release`. If the project still has not been deployed for some reason, you can checkout the repository at the latest release tag (e.g. `trivoreid-sdk-2.2.1`) and run `mvn deploy -P release`.
